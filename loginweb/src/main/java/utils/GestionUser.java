package utils;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.User;

public class GestionUser {
	private Session session;

	public GestionUser(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}
	
	public User findUser(String login, String password) {
		Query<User> query = this.session.createQuery("Select u from User u where u.login = :login and u.password = :password",User.class);
		query.setParameter("login", login);
		query.setParameter("password", password);
		
		if (query.list().size()!=0)
			return query.list().get(0);
		return null;
	}
	
	
	
	public void insertUser(User u) {
		this.session.save(u);
	}
	
	public String returnNombre(User u) {
		return u.getNombre()+" - "+u.getApellidos();
			
	}
	
}
