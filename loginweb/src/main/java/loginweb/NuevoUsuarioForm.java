package loginweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import model.User;
import utils.GestionUser;
import utils.HibernateUtils;

@WebServlet(value = "/NuevoUsuarioForm")
public class NuevoUsuarioForm extends HttpServlet {
	private final static String cabecera ="<html><head></head><body>";
	private final static String cierre="</body></html>";
	private static String HTML = "<!DOCTYPE html>\r\n" + "<html>\r\n" + "   <body>\r\n"
			+ "         <div><label>Crear nuevo usuario</label></div><br/>\r\n"
			+ "      <form action = \"NuevoUsuarioForm\" method = \"POST\">\r\n"
			+ "         Login: <input type = \"text\" name = \"login\" placeholder=\"id login\">  \r\n"
			+ "         <br />\r\n"
			+ "         Password: <input type = \"text\" type=\"password\" name = \"password\" placeholder=\"password usuario\"/>\r\n"
			+ "         <br />\r\n"
			+ "         Nombre: <input type = \"text\" name = \"nombre\" placeholder=\"nombre user\">  \r\n"
			+ "         <br />\r\n"
			+ "         Apellidos: <input type = \"text\" name = \"apellidos\" placeholder=\"apellidos\">  \r\n"
			+ "         <br />\r\n" + "<input type = \"submit\" value = \"Crear\" /><br />\r\n"
			+ "         <a href=\"./Login\">Ir al login</a>\r\n" + "      </form>\r\n" + "   </body>\r\n"
			+ "</html>\r\n" + "";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");

		// Actual logic goes here.
		PrintWriter out = response.getWriter();
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
		out.println(docType + HTML);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			PrintWriter out = resp.getWriter();
			session.beginTransaction();
			GestionUser gu = new GestionUser(session);
			User nuevoUser;
			
			if(gu.findUser(req.getParameter("login"), req.getParameter("password"))==null && comprobarFormulario(req)) {
				gu.insertUser(nuevoUser = new User(req.getParameter("login"),req.getParameter("password"),req.getParameter("nombre"),req.getParameter("apellidos")));
				
				session.getTransaction().commit();
				
				out.println(cabecera+"Usuario creado correctamente\n");
				out.println("<a href=\"./Login\">Ir al login</a>\r\n"+cierre );
			
			}else {
				out.println(cabecera+"error al crear usuario");
				out.println("<a href=\"./NuevoUsuarioForm\">Intentalo de nuevo</a>\r\n"+cierre );
				session.getTransaction().commit();
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} 
	}
	
	
	public static boolean comprobarFormulario(HttpServletRequest req) {
		if(req.getParameter("login").equals("") || req.getParameter("password").equals("") || req.getParameter("nombre").equals("") || req.getParameter("").equals(""))
			return false;
		return true;
	}

}
