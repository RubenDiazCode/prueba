package loginweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = "/Logout")
public class LogoutForward extends HttpServlet{
	private final static String cabecera ="<html><head></head><body>";
	private final static String cierre="</body></html>";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		HttpSession sesionHttp = req.getSession();
		cerrarSesion(sesionHttp);
		out.println(cabecera+"<p> Has cerrado sesión correctamente</p>");
		out.println("</p><p><a href=\"./Login\" >Volver al login</a>\r\n</p>"+cierre );
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(req, resp);
	}
	
	public static void cerrarSesion(HttpSession session) {
		session.invalidate();
	}

}


