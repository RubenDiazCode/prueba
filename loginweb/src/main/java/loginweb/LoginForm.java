package loginweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;

import utils.GestionUser;
import utils.HibernateUtils;

@WebServlet(value = "/Login")
public class LoginForm extends HttpServlet {
	private final static String cabecera ="<html><head></head><body>";
	private final static String cierre="</body></html>";
	private static String HTML = "<!DOCTYPE html>\r\n" + "<html>\r\n" + "   <body>\r\n"
			+ "         <div><label>Id login o password equivocados</label></div><br/>\r\n"
			+ "      <form action = \"Login\" method = \"POST\">\r\n"
			+ "         Login: <input type = \"text\" name = \"login\" placeholder=\"id login\">  \r\n"
			+ "         <br />\r\n"
			+ "         Password: <input type = \"text\" type=\"password\" name = \"password\" placeholder=\"password usuario\"/>\r\n"
			+ "         <br />\r\n" + "         <input type = \"submit\" value = \"Login\" /><br />\r\n"
			+ "         <a href=\"./NuevoUsuarioForm\">Crear nuevo usuario</a>\r\n" + "      </form>\r\n"
			+ "   </body>\r\n" + "</html>\r\n" + "";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");

		// Actual logic goes here.
		PrintWriter out = response.getWriter();
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
		out.println(docType + HTML);
		//Session session=HibernateUtils.getSessionFactory().openSession();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();

		String login = req.getParameter("login");

		String pass = req.getParameter("password");
		Session session=HibernateUtils.getSessionFactory().openSession();

		try {
			
			session.beginTransaction();
			GestionUser gu = new GestionUser(session);
			if (gu.findUser(login, pass)!=null) {
				session.getTransaction().commit();
				HttpSession sesionHttp =req.getSession();
				sesionHttp.setAttribute("LoggedUser",gu.findUser(login,pass));
				
			    req.getRequestDispatcher("./Welcome").forward(req, resp); 
			
			//	out.println(cabecera+""+login+" - "+pass+""+cierre );
			}else {
				out.println(cabecera+
						"<h2>User/pass incorrectos</h2>"+
						"<a href=\"./Login\">Volver al login</a>\r\n"+cierre );
			}
			
		//	session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} 

	}
}
