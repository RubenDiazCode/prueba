package loginweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = "/Welcome")
public class WelcomeForm extends HttpServlet {
	private final static String cabecera ="<html><head></head><body>";
	private final static String cierre="</body></html>";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();
		HttpSession sesionHttp = req.getSession();
		out.println(cabecera+"<p>Bienvenido <p><p>"+sesionHttp.getAttribute("LoggedUser").toString());
		out.println("</p><p><a href=\"./Logout\" >Cerrar sesión</a>\r\n</p>"+cierre );
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(req, resp);
	}
	
	

	
	

}
