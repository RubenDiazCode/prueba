package vehiculos;

public class Camion {
	private String modelo;
	private int cargaMaxima;

	public Camion() {

	}

	public Camion(String modelo, int cargaMaxima) {
		this.modelo = modelo;
		this.cargaMaxima = cargaMaxima;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getCargaMaxima() {
		return this.cargaMaxima;
	}

	public void setCargaMaxima(int cargaMaxima) {
		this.cargaMaxima = cargaMaxima;
	}

	public void cosasCamion() {
		System.out.println("soy un camion. Hago cosas.");
	}

}
