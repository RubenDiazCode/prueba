package vehiculos;

public class Coche {
	private String marca;
	private int caballos;

	public Coche() {

	}

	public Coche(String marca, int caballos) {
		this.marca = marca;
		this.caballos = caballos;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getCaballos() {
		return this.caballos;
	}

	public void setCaballos(int caballos) {
		this.caballos = caballos;
	}
	
	public void cosasCoche() {
		System.out.println("ERA UN DOMINGO EN LA TARDE Y FUI A LOS COCHE DE CHOKE");
		
	}

	

}
