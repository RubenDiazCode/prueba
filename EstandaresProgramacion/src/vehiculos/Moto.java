package vehiculos;

public class Moto {
	private String modelo;
	private String cilindrada;

	public Moto() {

	}

	public Moto(String modelo, String cilindrada) {
		this.modelo = modelo;
		this.cilindrada = cilindrada;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getCilindrada() {
		return this.cilindrada;
	}

	public void setCilindrada(String cilindrada) {
		this.cilindrada = cilindrada;
	}

	public void cosasMoto() {
		System.out.println("soy una moto, hago cosas");
	}

}
