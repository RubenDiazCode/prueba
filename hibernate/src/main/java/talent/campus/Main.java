package talent.campus;

import java.io.IOException;
import java.util.Random;

import org.hibernate.Session;

import talent.campus.dao.DaoAlumno;
import talent.campus.domain.Alumno;
import talent.campus.util.HibernateUtils;

public class Main {

	public static void main(String[] args) throws IOException {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			DaoAlumno daoAlumno = new DaoAlumno(session);
			
			Alumno alumno = daoAlumno.findById(1);
			System.out.println("Find-"+alumno.toString());

			
			System.out.println("Find findLikeNombre() despues Delete");
			daoAlumno.findLikeNombre("a")
				.forEach(alu -> System.out.println(alu.toString()));
			
			String oldName = alumno.getNombre();
			alumno.setNombre(alumno.getNombre()+"-"+ new Random().nextInt());
			daoAlumno.update(alumno);
			System.out.println("Update-"+alumno.toString());
			
			alumno.setNombre(oldName);
		    System.out.println("Restore old name. Rows affected: " + daoAlumno.updateQuery(alumno));
			
			Alumno alumnoInsertar1 = new Alumno("Temporal");
			daoAlumno.insert(alumnoInsertar1);
			System.out.println("Insert-"+alumnoInsertar1.toString());
			
			System.out.println("Find All() despues Insert");
			daoAlumno.findAll()
				.forEach(alu -> System.out.println(alu.toString()));

			daoAlumno.delete(alumnoInsertar1);
			
			System.out.println("Find All() despues Delete");
			daoAlumno.findAll()
				.forEach(alu -> System.out.println(alu.toString()));
			
			session.getTransaction().commit();			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
			HibernateUtils.shutdown();
		}
	}

}
