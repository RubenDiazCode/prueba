package talent.campus;

import java.io.IOException;

import org.hibernate.Session;

import talent.campus.domain.Rol;
import talent.campus.domain.Usuario;
import talent.campus.util.HibernateUtils;

public class MainUsuario {

	public static void main(String[] args) throws IOException {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			
			System.out.println("Test usuario id=1");
			Usuario usuario = session.get(Usuario.class, 1);
			System.out.println(usuario);
			System.out.println(String.format("Roles usuario[%s]:", usuario.getNombre()));
			usuario.getRoles().forEach(rol -> System.out.println(rol));
			

			System.out.println("Test rol id=2");
			Rol rol = session.get(Rol.class, 2);
			System.out.println(rol);
			System.out.println(String.format("Usuarios del rol[%s]:", rol.getNombre()));
			rol.getUsuarios().forEach(us -> System.out.println(us));

			System.out.println("Eliminar rol 2 de la base de datos y añado nuevo rol");

			Rol rolNuevo = new Rol("Nuevo rol");
			session.save(rolNuevo);
			
			Rol rol1 = usuario.getRoles().stream().filter(rolInUsuario -> rolInUsuario.getIdRol() == 1).findFirst().get();
			usuario.addRole(rolNuevo);
			usuario.removeRol(rol1);
			session.save(usuario);
			session.flush();
			
			session.save(usuario);
			
			session.getTransaction().commit();			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
			HibernateUtils.shutdown();
		}
	}

}
