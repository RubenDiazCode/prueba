package talent.campus.dao;

import org.hibernate.Session;

import talent.campus.domain.Asignatura;

public class DaoAsignatura {

	private Session session;

	public DaoAsignatura(Session session) {
		this.session = session;
	}

	public Asignatura findById(Integer id) {
		return this.session.load(Asignatura.class, id);
	}
}
