package talent.campus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import talent.campus.domain.Alumno;

public class DaoAlumno {

	private Session session;

	public DaoAlumno(Session session) {
		this.session = session;
	}

	public int updateQuery(Alumno alumno) {
		Query<Alumno> query = this.session.createQuery("UPDATE Alumno a SET a.nombre = :nombre WHERE a.id = :id");
	    query.setParameter("id",alumno.getId());
	    query.setParameter("nombre",alumno.getNombre());
	    return query.executeUpdate();		
	}
	
	public List<Alumno> findLikeNombre(String nombre) {
		Query<Alumno> query = this.session.createQuery("SELECT a FROM Alumno a WHERE a.nombre LIKE :nombre",Alumno.class);
		query.setParameter("nombre", "%"+nombre+"%");
		return query.list();
	}
	
	public List<Alumno> findByNombreAsignatura(String nombre){
		Query<Alumno> query = this.session.createQuery("SELECT a FROM Alumno a JOIN a.asignatura al WHERE al.nombre = :nombre",Alumno.class);
		query.setParameter("nombre", nombre);
		return query.list();
	
	}
	
	public List<Alumno> findAll() {
		Query<Alumno> query = this.session.createQuery("SELECT a FROM Alumno a",Alumno.class);
		return query.list();
	}

	public Alumno findById(Integer id) {
		return this.session.load(Alumno.class, id);
	}

	public void insert(Alumno alumno) {
		this.session.save(alumno);
	}

	public void update(Alumno alumno) {
		this.session.update(alumno);
	}

	public void delete(Alumno alumno) {
		this.session.delete(alumno);
	}
}
