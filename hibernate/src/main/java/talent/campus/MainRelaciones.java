package talent.campus;

import java.io.IOException;

import org.hibernate.Session;

import talent.campus.dao.DaoAlumno;
import talent.campus.dao.DaoAsignatura;
import talent.campus.domain.Alumno;
import talent.campus.domain.Asignatura;
import talent.campus.util.HibernateUtils;

public class MainRelaciones {

	public static void main(String[] args) throws IOException {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			DaoAlumno daoAlumno = new DaoAlumno(session);
			DaoAsignatura daoAsignatura = new DaoAsignatura(session);
			
			/* Pruebas con relaciones */
			Alumno alumno = daoAlumno.findById(1);
			System.out.println("Find-"+alumno.toString());
			
			System.out.println("Asignatura-"+alumno.getAsignatura().toString());
			
			Asignatura asignatura = daoAsignatura.findById(1);
			System.out.println("Asignatura: " + asignatura.toString());
			
			asignatura.getAlumnos().forEach(alu -> System.out.println(alu.toString()));
			
			session.getTransaction().commit();			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
			HibernateUtils.shutdown();
		}
	}

}
