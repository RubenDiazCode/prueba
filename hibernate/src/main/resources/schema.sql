DROP TABLE alumno;
DROP TABLE asignatura;

CREATE TABLE asignatura  (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(255)
);

CREATE TABLE alumno  (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nombre VARCHAR(255),
    id_asignatura INT,
    foreign key (id_asignatura) references asignatura(id)
);

INSERT INTO asignatura (id, nombre) VALUES (1, 'Java');
INSERT INTO asignatura (id, nombre) VALUES (2, 'Base de Datos');


INSERT INTO alumno (id, nombre, id_asignatura) VALUES (1, 'Raf', 1);
INSERT INTO alumno (id, nombre, id_asignatura) VALUES (2, 'Ana', 2);
INSERT INTO alumno (id, nombre, id_asignatura) VALUES (3, 'Jose', 1);

COMMIT;

DROP TABLE usuario_rol;
DROP TABLE rol;
DROP TABLE usuario;

CREATE TABLE usuario(
    id_usuario INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	login VARCHAR(255) NOT NULL UNIQUE,
	nombre_usuario VARCHAR(255)
);

CREATE TABLE rol (
    id_rol INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	nombre_rol VARCHAR(255) NOT NULL
);

CREATE TABLE usuario_rol (
    id_rol INT NOT NULL,
    id_usuario INT NOT NULL,
    foreign key (id_rol) references rol(id_rol),
    foreign key (id_usuario) references usuario(id_usuario),
    PRIMARY KEY (id_rol,id_usuario)
);

INSERT INTO usuario(id_usuario,login,nombre_usuario) VALUES (1,'login1','Nombre 1');
INSERT INTO usuario(id_usuario,login,nombre_usuario) VALUES (2,'login2','Nombre 2');

INSERT INTO rol(id_rol, nombre_rol) VALUES (1,'Rol 1');
INSERT INTO rol(id_rol, nombre_rol) VALUES (2,'Rol 2');

INSERT INTO usuario_rol(id_usuario,id_rol) VALUES (1,1,1);
INSERT INTO usuario_rol(id_usuario,id_rol) VALUES (2,1,2);
INSERT INTO usuario_rol(id_usuario,id_rol) VALUES (3,2,2);

COMMIT;
