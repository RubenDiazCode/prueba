package prueba_maven2;

import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PersonaWriter implements Serializable {
	private List<Persona> lista;
	File file;

	public PersonaWriter() {
		this.lista = new ArrayList<Persona>();
		this.file = new File("./ficheroPersonas.csv");
	}

	public List<Persona> getLista() {
		return this.lista;
	}

	public void setLista(List<Persona> lista) {
		this.lista = lista;
	}

	//modificado para el ejercicio 4, antes no pedia nada
	public void write(List<Persona> lista) {
		try {
			FileWriter fw = new FileWriter(this.file);
			for (Persona p : lista) {
				fw.write(p.toString());
			}

			fw.close();
		} catch (Exception ex) {
			System.out.println("YA LA HAS LIADO CON EL FICHERO ESCRIBIENDO");
		}

	}

	

}
