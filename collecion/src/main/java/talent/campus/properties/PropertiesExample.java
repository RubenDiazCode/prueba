package talent.campus.properties;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesExample {

	public static void main(String[] args) throws IOException {
		Properties prop = leerPropriedades();
		printeValorePropriedadesLeidas(prop);
		prop.putIfAbsent("author", "Raf");
		persistirFicheroConf(prop);
	}

	private static Properties leerPropriedades() throws IOException {
		Properties prop = new Properties();
		InputStream is = PropertiesExample.class.getClassLoader().getResourceAsStream("application.properties");
		prop.load(is);
		return prop;
	}

	private static void printeValorePropriedadesLeidas(Properties prop) {
		// Acceder a las propiedades por su nombre
		System.out.println("Propiedades por nombre:");
		System.out.println("-----------------------");
		System.out.println(prop.getProperty("servidor.url"));
		System.out.println(prop.getProperty("servidor.username"));
		System.out.println(prop.getProperty("servidor.usuario"));

		// Recorrer todas sin conocer los nombres de las propiedades
		System.out.println("Recorrer todas las propiedades:");
		System.out.println("-------------------------------");

		for (Object pro : prop.keySet()) {
			System.out.println(pro + ": " + prop.getProperty(pro.toString()));
		}
	}

	private static void persistirFicheroConf(Properties prop) throws IOException {
		String dirBase = System.getProperty("user.home");
		String fileName = dirBase + "/application.properties";

		System.out.println("Escribo fichero in: " + fileName);

		File file = new File(fileName);
		FileWriter fichero = new FileWriter(file);
		prop.store(fichero, "Cambio Raf!!");
		fichero.close();
	}

}
