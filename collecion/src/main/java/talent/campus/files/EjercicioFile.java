package talent.campus.files;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class EjercicioFile {

	public void write(String fileName) throws IOException {
		File file = new File(fileName);
		FileWriter fichero = new FileWriter(file);
		fichero.write("Raffy");
		fichero.close();
	}

	public void read(String fileName) throws IOException {
		FileReader fichero = new FileReader(fileName);
		try {
			char block[] = new char[1024];
			while (fichero.read(block, 0, block.length) != -1) {
				System.out.print(block);
			}
		} finally {
			fichero.close();
		}

	}

	public static void main(String[] args) throws IOException {
		String dirBase = System.getProperty("java.io.tmpdir");

		String fileName = dirBase + "/nombre.txt";
		EjercicioFile ejercicio = new EjercicioFile();
		ejercicio.write(fileName);
		ejercicio.read(fileName);

		System.out.println("End");
	}

}
