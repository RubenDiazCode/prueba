package talent.campus.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class EjercicioBufferedFile {

	public void write(String fileName, String nombre) {
		try (FileWriter writer = new FileWriter(fileName); BufferedWriter buf = new BufferedWriter(writer)) {
			buf.write(nombre);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void read(String fileName) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(fileName));

			String line = "";
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {
		String dirBase = System.getProperty("java.io.tmpdir");
		String fileName = dirBase + File.separator + "nombre2.txt";

		EjercicioBufferedFile ejercicio = new EjercicioBufferedFile();
		ejercicio.write(fileName, "Raf");
		ejercicio.read(fileName);

		System.out.println("End");

	}

}
