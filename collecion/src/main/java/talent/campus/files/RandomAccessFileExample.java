package talent.campus.files;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
 
public class RandomAccessFileExample {
 	
    
    public static void main(String[] args) {
        try {
    		URL resource = RandomAccessFileExample.class.getClassLoader().getResource("tmp2.txt");
    		String filePath = resource.getPath();
    		
            System.out.println(new String(readFromFile(filePath, 150, 23)));
 
            writeToFile(filePath, "Raffy Rocks!", 22);
           // System.out.println(new String(readFromFile(FILEPATH, 0, 100)));
        } catch (IOException e) {
            e.printStackTrace();
        }
 
    }
 
    private static byte[] readFromFile(String filePath, int position, int size)
            throws IOException {
 
        RandomAccessFile file = new RandomAccessFile(filePath, "r");
        System.out.println("Ultimo byte leido: "+file.getFilePointer());
        file.seek(position);
        System.out.println("Ultimo byte leido: "+file.getFilePointer());
        byte[] bytes = new byte[size];
        file.read(bytes);
        System.out.println("Ultimo byte leido: "+file.getFilePointer());
        file.close();
        return bytes;
 
    }
 
    private static void writeToFile(String filePath, String data, int position)
            throws IOException {
 
        RandomAccessFile file = new RandomAccessFile(filePath, "rw");
        System.out.println("Ultimo byte grabado: "+file.getFilePointer());
        file.seek(position);
        System.out.println("Ultimo byte grabado: "+file.getFilePointer());
        file.write(data.getBytes());
        System.out.println("Ultimo byte grabado: "+file.getFilePointer());
        file.close();
 
    }
}