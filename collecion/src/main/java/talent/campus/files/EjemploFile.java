package talent.campus.files;

import java.io.File;
import java.io.IOException;

public class EjemploFile {

	public static void main(String[] args) throws IOException {
		String tmpDir = System.getProperty("java.io.tmpdir");
		
		File file = new File(tmpDir+ File.separator + "prueba.txt"); //separator=buenas practicas
		System.out.println("File " + file.getAbsolutePath() + " existe? => " + file.exists());
		
		File directory = new File(tmpDir+ File.separator + "dir_prueba");
		if(! directory.exists()) {
			System.out.println("Crear carpeta " +  directory.getAbsolutePath());
			System.out.println("Resultado "+ directory.mkdir());
		}
		
		File tmp = File.createTempFile("prueba_", ".txt", directory);
		System.out.println("File " + tmp.getAbsolutePath() + " existe? => " + tmp.exists());
		
		System.out.println("Directory? " + directory.isDirectory());
		System.out.println("File? " + tmp.isFile());
		
		//File.listRoots()
		System.out.println("Fichero borrado? => " + tmp.delete());
	}

}
