package talent.campus.files;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class LecturaFichero {

	public static void main(String[] args) throws IOException {
		String fileName = System.getProperty("java.io.tmpdir") + File.separator + "nombre2.txt";
		File file = new File(fileName);

		FileReader fichero = new FileReader(file);
		try {
			int ch = fichero.read();
			while (ch != -1) {
				System.out.print((char) ch);
				ch = fichero.read();
			}

		} finally {
			fichero.close();
		}
	}

}
