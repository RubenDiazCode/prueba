package talent.campus.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class EjemploFileStream {
//mal
	public void write(String fileName, String nombre) throws IOException {
		OutputStream output = new FileOutputStream(fileName);
		output.write(nombre.getBytes());
		output.close();
	}

	// bien
	public void read(String fileName) throws IOException {
		try (InputStream inputStream = new FileInputStream(fileName)) {
			int available = inputStream.available();
			System.out.println("Caracteres disponibles: " + available);

			byte[] contenido = new byte[available];
			inputStream.read(contenido);

			System.out.println(new String(contenido));
		}
	}

	public static void main(String[] args) throws IOException {
		String dirBase = System.getProperty("java.io.tmpdir");
		String fileName = dirBase + File.separator + "nombre3.txt";

		EjemploFileStream ejercicio = new EjemploFileStream();
		ejercicio.write(fileName, "Raf");
		ejercicio.read(fileName);

		System.out.println("End");

	}

}
