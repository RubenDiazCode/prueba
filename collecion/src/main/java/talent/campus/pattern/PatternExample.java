package talent.campus.pattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternExample {

	public static void main(String[] args) {
		Pattern pattern = Pattern.compile("a*b");
		Matcher match = pattern.matcher("aaaaab");
		String resultado = match.replaceAll("-");
		System.out.println(resultado);
		
		
		validarServidor("www.ddd.es");
		
		validarCodigoPostal("03006");
		validarCodigoPostal("030066");
		validarCodigoPostal("0300");
		validarCodigoPostal("03X66");
		
		validarEmail("raf@");
		validarEmail("raf@gmail");
		validarEmail("raf@gmail.com");
	}

	private static void validarEmail(String input) {
		Matcher match1 = Pattern.compile("^[A-Za-z0-9+_.-]+@(a-zA-Z0-9.-)$").matcher(input);		
		if( match1.matches() ) {
			System.out.println(String.format("Email valido [%s]", input));
		} else {
			System.out.println(String.format("Email no valido [%s]", input));
		}
	}
	
	private static void validarServidor(String input) {
		Matcher match1 = Pattern.compile("^www.*.es").matcher(input);		
		if( match1.matches() ) {
			System.out.println("Página web de un servidor Español");
		} else {
			System.out.println("Otra página web");
		}
	}
	
	private static void validarCodigoPostal(String input) {
		Matcher match1 = Pattern.compile("[0-9]{5}").matcher(input);		
		if( match1.matches() ) {
			System.out.println(String.format("Codigo postal valido [%s]", input));
		} else {
			System.out.println(String.format("Codigo postal no valido [%s]", input));
		}
	}
}
