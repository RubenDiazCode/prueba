package talent.campus.collecion;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class ArrayListExample {

	public static void main(String[] args) {
				
		List<String> coleccion = new ArrayList<String>();
	
		System.out.println("Empty: " + coleccion.isEmpty());
		
		coleccion.add("Item-1");
		System.out.println("¿Contiene el elemento? " + coleccion.contains("Item-3"));
		coleccion.addAll(Arrays.asList("Item-2", "Item-3", "Item-4"));
		System.out.println("¿Contiene el elemento? " + coleccion.contains("Item-3"));
		
		System.out.println("Size: " + coleccion.size());
		
		
		coleccion.add(2, "raf");
		print(coleccion);
		
		System.out.println("Remove element");
		coleccion.remove("raf");
		print(coleccion);
		
		System.out.println("indexOf: " + coleccion.indexOf("Item-3"));
		coleccion.add("Item-3");
		System.out.println("indexOf: " + coleccion.indexOf("Item-3"));
		System.out.println("lastIndexOf: " + coleccion.lastIndexOf("Item-3"));
		
		coleccion.add(0, "Item-5");
		coleccion.sort(Comparator.naturalOrder());
		print(coleccion);

		coleccion.clear();
		print(coleccion);
		
	}

	private static void print(List<String> lista) {
		if(lista.isEmpty()) {
			System.out.println("Empty list");
		} else {
			lista.forEach(item -> System.out.print(item + " "));//LAMBDA POGCHAMP
			System.out.println();
		}
	}
}
