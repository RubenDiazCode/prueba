package talent.campus.collecion;

import java.util.HashMap;
import java.util.Map;

public class HashMapExample<T> {
	private T objeto;
	public static void main(String[] args) {
		Map<Integer, String> collecion = new HashMap<Integer, String>();
		
		print(collecion);
		
		collecion.put(10, "Item-10");
		collecion.put( 1, "Item-1");
		collecion.put( 6, "Item-6");
		//No se admiten duplicados
		collecion.put( 6, "Item-16");
		
		print(collecion);
		
		System.out.println("Contain key: " + collecion.containsKey(7));
		System.out.println("Contain key: " + collecion.containsValue("Item-7"));
		collecion.put( 7, "Item-7");
		System.out.println("Contain key: " + collecion.containsKey(7));
		System.out.println("Contain key: " + collecion.containsValue("Item-7"));
		
		System.out.println("Size: " + collecion.size());
		
		System.out.println();
		System.out.println("Update element");
		collecion.replace(7, "Item-7 Update");
		print(collecion);

		System.out.println();
		System.out.println("Remove element");
		collecion.remove(7);
		print(collecion);
		
		System.out.println();
		System.out.println("Clear elements");
		collecion.clear();
		print(collecion);

	}
	
	private static void print(Map<Integer, String> hashMap) {
		if(hashMap.isEmpty()) {
			System.out.println("Empty map");
		} else {
			for(Integer key : hashMap.keySet()) {
				System.out.println(key + " => " + hashMap.get(key));
			}
		}
	}

}
