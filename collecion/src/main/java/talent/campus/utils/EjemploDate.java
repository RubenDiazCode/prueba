package talent.campus.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EjemploDate {

	public static void main(String[] args) {
		Date fecha = new Date();
		System.out.println(fecha);
		
		System.out.println("Numero de milisegundo desde January 1, 1970, 00:00:00 GMT ==> "+ fecha.getTime());

		Date fecha2 = new Date(118, 5, 3);
		System.out.println(fecha2);

		Date fecha3 = new Date(116, 5, 3, 10, 5, 6);
		System.out.println(fecha3);

		System.out.println("Año: " + (fecha3.getYear() + 1900));
		System.out.println("Mes: " + fecha3.getMonth());
		System.out.println("Dia: " + fecha3.getDate());
		System.out.println("Dia de la semana " + fecha3.getDay());
		System.out.println("Hora: " + fecha3.getHours());
		System.out.println("Minutos: " + fecha3.getMinutes());
		System.out.println("Segundos: " + fecha3.getSeconds());

		
		SimpleDateFormat dateformatter = new SimpleDateFormat("dd/MM/yyyy hh:ss:mm");
		
		System.out.println("¿"+dateformatter.format(fecha3) + " es anterior a " + dateformatter.format(fecha)+ "? Respuesta: " + fecha3.before(fecha));
		System.out.println("¿"+dateformatter.format(fecha3) + " viene después de " + dateformatter.format(fecha)+ "? Respuesta: " + fecha3.after(fecha));
		
		
	}

}
