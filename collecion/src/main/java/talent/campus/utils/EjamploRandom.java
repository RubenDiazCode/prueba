package talent.campus.utils;

import java.util.Iterator;
import java.util.Random;
import java.util.stream.IntStream;

public class EjamploRandom {

	public static void main(String[] args) {
		
		generarConMath();
		generarConClaseRandom();
		generarIteradorDeNumerosRandom();
	}

	private static void generarConMath() {
		System.out.println("Genedador con Math");
		System.out.println();
		for (int i = 0; i < 10; i++) {
			double random = Math.random();
			System.out.printf("Generado: %f \n" , random);
		}
		System.out.println();
	}

	private static void generarConClaseRandom() {
		System.out.println("Genedador con clase Random");
		System.out.println();
		Random r = new Random();
		for (int i = 0; i < 10; i++) {
			int valorDado = r.nextInt(6) + 1;
			System.out.println("Generado [1-6]: " + valorDado);
		}
		System.out.println();
	}

	private static void generarIteradorDeNumerosRandom() {
		System.out.println("Genedador Iterador Numeros Random");
		System.out.println();
		// Instanciar clase Random
		Random random = new Random();

		// Obtener IntStream. El IntStream tendrá 10 números aleatorios
		// entre 1 y 7, excluido el 7. Vaya, la típica tirada de dados del 1 al 6.
		IntStream intStream = random.ints(10, 1, 7);

		// Iterador para ir obteniendo los números
		Iterator<Integer> iterator = intStream.iterator();

		// Sacamos los números aleatorios por pantalla, en un bucle.
		while (iterator.hasNext()){
		   System.out.println("Random Number "+iterator.next());
		}
	}

}
