package cosas.stack;

public class Stack {

	int[] array;
	int puntero;

	public Stack(int tamanyo) throws ItemException {
		if (tamanyo <= 0)
			throw new ItemException("tama�o de registro no valido");

		this.array = new int[tamanyo];
		this.puntero = 0;
	}

	public void push(int item) throws ItemException {
		if (this.puntero == this.array.length) {
			throw new ItemException("Array lleno");
		}
		this.array[this.puntero] = item;
		this.puntero++;
		
	}

	public int pop() {
		int item = this.array[this.puntero];
		this.array[this.puntero]=0;
		this.puntero--;
		return item;
	}
	
	public int top() throws ItemException {
		if(this.puntero==0)
			throw new ItemException("array vac�o");
		return this.array[this.puntero];
	}
	
	public boolean isEmpty() {
		if(this.puntero==0)
			return true;
		return false;
	}
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Stack s=new Stack(3);
			s.push(3);
			s.push(45);
			
			System.out.println(s.pop());
			s.push(-2);
			System.out.println(s.top());		
			System.out.println(s.isEmpty());
			for(int i:s.array) {
				System.out.println(i);
			}
		} catch (ItemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
