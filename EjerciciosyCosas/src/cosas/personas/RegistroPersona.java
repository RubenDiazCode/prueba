package cosas.personas;

import java.util.ArrayList;

public class RegistroPersona {
	   private int tamanyoLista;
	   private Persona[] lista;

	   public RegistroPersona(int tamanyoLista)  {

//	        if (this.tamanyoLista <= 0)
//	            throw new NegativoException("has metido un numero negativo de gente");
		   this.tamanyoLista = tamanyoLista;
	       this.lista = new Persona[this.tamanyoLista];
	   }

	   public int getTamanyoLista() {
	       return this.tamanyoLista;
	   }

	   public void setTamanyoLista(int tamanyoLista) {
	       this.tamanyoLista = tamanyoLista;
	   }

	   public Persona[] getLista() {
	       return this.lista;
	   }

	   public int buscaPorDni(String dni){
	       for(int i=0;i<this.tamanyoLista;i++){
	           if(this.lista[i].getDni().equalsIgnoreCase(dni))
	               return i;
	       }
	       return -1;
	   }

	   public String add(Persona p) {
	       for (int i = 0; i < this.tamanyoLista; i++) {
	           if (this.lista[i] == null){
	               this.lista[i] = p;
	               return "persona a�adida en la posicion " + i;
	           }
	           
	       }
	       return "aqui deberia haber una exception de array lleno";
	   }

	   public Persona find(String dni){
	       int posicion= buscaPorDni(dni);
	       if(posicion !=-1)
	           return this.lista[posicion];
	       return null;
	   }
	   public boolean contains(String dni){
	       if(buscaPorDni(dni)!=-1)
	           return true;
	       return false;
	   }

	   public Persona remove(String dni){
	       Persona p;
	       int posicion= buscaPorDni(dni);
	       if(posicion !=-1){
	           p=new Persona(this.lista[posicion].getDni(),this.lista[posicion].getNombre(),this.lista[posicion].getApellidos());
	           this.lista[posicion]=null;
	           return p;
	       }
	   return null;
	   }

	   public String[] getAll(){
	       String[] salida= new String[this.tamanyoLista];
	       for(int i=0;i<this.tamanyoLista;i++){
	    	   if(this.lista[i]==null) {
	    		   salida[i]="espacio vacio, borrar despues";
	    	   }else {    		   
	    		   salida[i]=this.lista[i].toString();
	    	   }
	       }
	       return salida;
	   }

	   
	}

