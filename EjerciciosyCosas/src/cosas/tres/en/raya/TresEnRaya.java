package cosas.tres.en.raya;

public class TresEnRaya {

	String[][] tablero;

	public TresEnRaya() {
		this.tablero = new String[3][3];
	}

	public void meterFicha(String ficha, int x, int y) {
		if (ficha.equalsIgnoreCase("X")) {
			this.tablero[x][y] = ficha;
		} else {
			ficha = "O";
			this.tablero[x][y] = ficha;
		}

	}

	public boolean casillaLibre(int x, int y) {
		if (this.tablero[x][y] != null)
			return false;
		return true;
	}

	public boolean hayGanador() {

		for (int i = 0; i < this.tablero.length; i++) {
			if (this.tablero[i][0].equals(this.tablero[i][1].equals(this.tablero[i][2])))
				return true;

			if (this.tablero[0][i].equals(this.tablero[1][i].equals(this.tablero[2][i])))
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
