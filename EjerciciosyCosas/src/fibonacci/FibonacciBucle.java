package fibonacci;

import java.util.Scanner;

public class FibonacciBucle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("iyo de cuanto la serie");
		int serie = sc.nextInt();
		int total = 0;
		int num1 = 0;
		int num2 = 1;

		for (int i = 0; i < serie; i++) {
			System.out.println(imprimeResultado(total));
			total = num1 + num2;
			num1 = num2;
			num2 = total;
		}

	}

	public static String imprimeResultado(int total) {
//		if(total==2)
//			return 1+"";
		if (total % 2 == 0)
			return total + "*";
		
		
		return total + "";

	}

}
//Serie de Fibonacci 
//
//Los n�meros de Fibonacci quedan definidos por las ecuaciones:
//
//fib(0) = 0
//fib(1) = 1
//
//fib(n) = fib(n-1) + fib(n-2)
//
//Esto produce los siguientes n�meros:
//fib(2) = 1
//fib(3) = 2
//fib(4) = 3
//fib(5) = 5
//fib(6) = 8
//fib(7) = 13
//fib(8) = 21
//
//Escribir una funci�n fibonacci(int n) que imprima en pantalla los n�meros de la serie de fibonacci desde 0 hasta n.
//
//En caso el n�mero sea par imprimir imprimir cerca del valor un *.
//Un ejemplo de output de la funci�n fibonacci(8) ser�a esto
//    
//0 *
//1
//1
//2 *
//3
//5
//8 *
//13
//21
