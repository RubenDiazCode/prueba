package es.devcenter.talent.campus.complex;

public class ArticleManager {
    
	private User user;
    
    private ArticleDatabase database;

    public ArticleManager(User user, ArticleDatabase database) {
        super();
        this.user = user;
        this.database = database;
    }

    public void initialize() {
        this.database.addListener(new ArticleListener());
        String name = this.user.getName() == null ? "Empty" :  this.user.getName();
        System.out.println( "User: " + name);
    }
}
