package es.devcenter.talent.campus.complex;

public class User {
	
	private String name;
	
	public User(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
