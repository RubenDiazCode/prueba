package es.devcenter.talent.campus.spy;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SpyRealObjectAndVerifyTest {

	@Spy
	HashMap<String, Integer> hashMap;
	 
	@Test
	public void testHashMap()
	{
	    this.hashMap.put("A", 10);
	     
	    Mockito.verify(this.hashMap, Mockito.times(1)).put("A", 10);
	    Mockito.verify(this.hashMap, Mockito.times(0)).get("A");
	     
	    Assert.assertEquals(1, this.hashMap.size());
	    Assert.assertEquals(new Integer(10), (Integer) this.hashMap.get("A"));
	}
}
