package es.devcenter.talent.campus.inject_mock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import es.devcenter.talent.campus.complex.ArticleDatabase;
import es.devcenter.talent.campus.complex.ArticleListener;
import es.devcenter.talent.campus.complex.ArticleManager;
import es.devcenter.talent.campus.complex.User;

@RunWith(MockitoJUnitRunner.class)
public class ArticleManagerTest {

    @Mock 
    private ArticleDatabase database;

    @Mock 
    private User user;

    @InjectMocks 
    private ArticleManager manager; 

    @Test 
    public void testInjectMocks() {
        this.manager.initialize();
        Mockito.verify(this.database).addListener(Mockito.any(ArticleListener.class));
    }
}
