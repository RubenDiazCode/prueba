package es.devcenter.talent.campus.spy;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class SpyRealObjectTest {

	@Test(expected = IndexOutOfBoundsException.class)
	public void testLinkedListSpyWrong() {
	    // Lets mock a LinkedList
	    List<String> list = new LinkedList<>();
	    List<String> spy = Mockito.spy(list);

	    // no funciona!!!
	    // se llama el metodo real spy.get(0)
	    // genera IndexOutOfBoundsException (la lista está todavia vacía)
	    Mockito.when(spy.get(0)).thenReturn("foo");

	    Assert.assertEquals("foo", spy.get(0));
	}
	
	@Test
	public void testLinkedListSpyCorrect() {
	    List<String> list = new LinkedList<>();
	    List<String> spy = Mockito.spy(list);

	    // Con spy se tiene que usar doReturn
	    Mockito.doReturn("foo").when(spy).get(0);

	    Assert.assertEquals("foo", spy.get(0));
	}
}
