package es.devcenter.talent.campus.create;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import es.devcenter.talent.campus.Prueba;


public class CreateMockTest {

	private Prueba prueba;
	
	@Before
	public void init() {
		this.prueba = Mockito.mock(Prueba.class);
		Mockito.when(this.prueba.getMensaje()).thenReturn("Mensaje mock");
	}
	
	@Test
	public void testMockedMessage() {		
		Assert.assertEquals("Mensaje mock", this.prueba.getMensaje());
	}
}
