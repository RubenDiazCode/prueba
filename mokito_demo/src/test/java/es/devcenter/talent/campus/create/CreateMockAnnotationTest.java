package es.devcenter.talent.campus.create;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import es.devcenter.talent.campus.Prueba;

@RunWith(MockitoJUnitRunner.class)
public class CreateMockAnnotationTest {
	
	@Mock
	private Prueba prueba;
	
	@Before
	public void init() {
		Mockito.when(this.prueba.getMensaje()).thenReturn("Mensaje mock");
	}
	
	@Test
	public void testMockedMessage() {		
		Assert.assertEquals("Mensaje mock", this.prueba.getMensaje());
	}
}
