package es.devcenter.talent.campus.create;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import es.devcenter.talent.campus.Prueba;

public class CreateMockAnnotationWithRuleTest {

	
	@Mock
	private Prueba prueba;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(this.prueba.getMensaje()).thenReturn("Mensaje mock");
	}
	
	@Test
	public void testMockedMessage() {		
		Assert.assertEquals("Mensaje mock", this.prueba.getMensaje());
	}
}
