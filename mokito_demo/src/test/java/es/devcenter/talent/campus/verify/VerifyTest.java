package es.devcenter.talent.campus.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class VerifyTest {

	@Mock
	private Prueba test;
	
	@Before
	public void init() {
		Mockito.when(this.test.getUniqueId()).thenReturn(43);
	}
	
	@Test
	public void testVerify() {
	    this.test.testing(12);
	    this.test.getUniqueId(); 
	    this.test.getUniqueId();
	    
	    Mockito.verify(this.test).testing(ArgumentMatchers.eq(12));
	    
	    Mockito.verify(this.test, Mockito.times(2)).getUniqueId();
	    
	    Mockito.verify(this.test, Mockito.never()).someMethod("never called");
	    this.test.someMethod("called at least once");
	    
	    Mockito.verify(this.test, Mockito.atLeastOnce()).someMethod("called at least once");

	    this.test.someMethod("called at least twice");
	    this.test.someMethod("called at least twice");
	    Mockito.verify(this.test, Mockito.atLeast(2)).someMethod("called at least twice");

	    this.test.someMethod("called five times");
	    this.test.someMethod("called five times");
	    this.test.someMethod("called five times");
	    this.test.someMethod("called five times");
	    this.test.someMethod("called five times");
	    
	    Mockito.verify(this.test, Mockito.times(5)).someMethod("called five times");

	    this.test.someMethod("called at most 3 times");
	    this.test.someMethod("called at most 3 times");
	  
	    Mockito.verify(this.test, Mockito.atMost(3)).someMethod("called at most 3 times");
	    
	    //Se puede usar para averiguar que no haya niguna otra interación 
	    Mockito.verifyNoMoreInteractions(this.test);
	}
	
}
