package es.devcenter.talent.campus.when;

import java.util.HashMap;
import java.util.Map;

public class Dictionary {
	
	private Map<String, String> map;
	
	public Dictionary() {		
		this.map = new HashMap<String, String>();
		this.map.put("key_1", "valor 1");
		this.map.put("key_2", "valor 2");
	}
	
	public String get(String key) {
		return this.map.get(key);
	}
	
	public String getVersion() {
		return "Ver 1.0";
	}

}
