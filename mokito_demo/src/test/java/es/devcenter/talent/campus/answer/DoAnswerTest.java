 package es.devcenter.talent.campus.answer;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class DoAnswerTest {

	@Test
	public void testDoAnswer() {
		ClienteCorreo clienteCorreo = Mockito.mock(ClienteCorreo.class);
		Mockito.doAnswer(invocation -> {
            String correo = invocation.getArgument(0);
            if("error".equalsIgnoreCase(correo)) {
            	return false;
            }

            return true;
		}).when(clienteCorreo).send(Mockito.anyString());
		
		
		Assert.assertTrue("Send Ok", clienteCorreo.send("ok"));
		Assert.assertFalse("Send Ok", clienteCorreo.send("error"));	
	}
}
