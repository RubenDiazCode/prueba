package es.devcenter.talent.campus.when;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.junit.Test;

public class EjemploWhenTest {
	

	@Test
	public void testMoreThanOneReturnValue() {
		Iterator<String> i = mock(Iterator.class);
		when(i.next()).thenReturn("Mockito").thenReturn("rocks");
		String result = i.next() + " " + i.next();

		assertEquals("Mockito rocks", result);
	}

	@Test
	public void testReturnValueDependentOnMethodParameter() {
		Comparable<String> c = mock(Comparable.class);
		when(c.compareTo("Mockito")).thenReturn(1);
		when(c.compareTo("Eclipse")).thenReturn(2);

		assertEquals(1, c.compareTo("Mockito"));
	}

	@Test
	public void testReturnValueInDependentOnMethodParameter() {
		Comparable<Integer> c = mock(Comparable.class);
		when(c.compareTo(anyInt())).thenReturn(-1);
		
		assertEquals(-1, c.compareTo(9));
	}

	@Test
	public void testReturnValueInDependentOnMethodParameter2() {
		Comparable<Todo> c = mock(Comparable.class);
		when(c.compareTo(isA(Todo.class))).thenReturn(0);

		assertEquals(0, c.compareTo(new Todo(1)));
	}
}
