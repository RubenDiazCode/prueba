package es.devcenter.talent.campus.when;

public class Todo {

	private int order;
	
	public Todo(int order) {
		this.order = order;
	}

	public int getOrder() {
		return this.order;
	}

}
