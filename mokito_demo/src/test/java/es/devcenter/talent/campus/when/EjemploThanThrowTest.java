package es.devcenter.talent.campus.when;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EjemploThanThrowTest {
	
	@Mock
	private Properties properties;
	
	@Before
	public void init() throws Exception {
		Mockito.when(this.properties.get("mysql.password")).thenThrow(new IllegalArgumentException("Prueba!!"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testThanThrow() throws Exception {		
		this.properties.get("mysql.password");

	}
}
