package es.devcenter.talent.campus.create;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import es.devcenter.talent.campus.Prueba;

public class CreateMockitoRuleTest {

	@Rule 
	public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	@Mock
	private Prueba prueba;
	
	@Before
	public void init() {
		Mockito.when(this.prueba.getMensaje()).thenReturn("Mensaje mock");
	}
	
	@Test
	public void testMockedMessage() {		
		Assert.assertEquals("Mensaje mock", this.prueba.getMensaje());
	}
}
