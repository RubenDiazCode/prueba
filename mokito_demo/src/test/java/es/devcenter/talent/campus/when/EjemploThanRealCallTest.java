package es.devcenter.talent.campus.when;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;


public class EjemploThanRealCallTest {
	
	@Test
	public void testThanThrow() throws Exception {		
		Dictionary dictionary = Mockito.mock(Dictionary.class);
				
		Mockito.when(dictionary.getVersion()).thenCallRealMethod();
		Mockito.when(dictionary.get("key_1")).thenReturn("value 1 moked");
		
		Assert.assertEquals("value 1 moked", dictionary.get("key_1"));
		Assert.assertEquals("Ver 1.0", dictionary.getVersion());

	}
}
