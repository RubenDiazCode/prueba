package es.devcenter.talent.campus.captor;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CaptorTest {

	
	@Mock
	private HashMap<String, Integer> hashMap;
	 
	@Captor
	private ArgumentCaptor<String> keyCaptor;
	 
	@Captor
	private ArgumentCaptor<Integer> valueCaptor;
	 
	@Test
	public void testCaptor() {
	    this.hashMap.put("A", 10);
	 
	    Mockito.verify(this.hashMap).put(this.keyCaptor.capture(), this.valueCaptor.capture());
	 
	    Assert.assertEquals("A", this.keyCaptor.getValue());
	    Assert.assertEquals(new Integer(10), this.valueCaptor.getValue());
	}
}
