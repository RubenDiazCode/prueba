package talent.campus.enumerados.ejemplo2;

import java.util.Scanner;

public class PruebaEnumerado {

	public static void main(String[] args) {
		Color color = Color.valueOf("ROJO");		
		System.out.println("Color:" + color);
		
		System.out.println("Introducir color:");
		Scanner scanner = new Scanner(System.in);
		String codigoColor = scanner.nextLine();
		

		for(Color color2 : Color.values()) {
			if(color2.getCodigo().equals(codigoColor)) {
				System.out.println("Color encontrado:" + color2);
			}
		}
	}

}
