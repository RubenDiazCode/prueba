package talent.campus.enumerados.ejemplo2;

public enum Color {
	ROJO("00"), VERDE("01"), AMARILLO("02");
	
	private final String codigo;
	
	private Color(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return this.codigo;
	}
	
}
