package talent.campus.enumerados.ejemplo1;

import ejercicios.Estacion;
import ejercicios.Mes;

public class Pruebas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for (Mes m : Mes.values()) {

			if (!(m.getEstacion().equals(Estacion.PRIMAVERA)))
				System.out.println(m + " Estacion: " + m.getEstacion());
		}
	}

}
