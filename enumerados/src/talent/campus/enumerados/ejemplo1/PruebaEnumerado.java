package talent.campus.enumerados.ejemplo1;

public class PruebaEnumerado {

	public static final void main(String[] args) {
		Transporte transporte = Transporte.COCHE;

		for (Transporte transporte2 : Transporte.values()) {
			System.out.println(transporte2.ordinal() + "-" + transporte2.name());
		}

		System.out.println("Transporte: " + transporte);

		switch (transporte) {
		case BARCO:
			System.out.println("Navega en el agua");
			break;

		case COCHE:
			System.out.println("Lleva personas");
			break;
		case TREN:
			System.out.println("Corre sobre railes");
			break;

		default:
			System.err.println("Error, valor no admitido");
			break;
		}

	}

}
