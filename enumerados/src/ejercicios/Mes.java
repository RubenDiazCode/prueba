package ejercicios;

public enum Mes {
	ENERO(Estacion.INVIERNO), FEBRERO(Estacion.INVIERNO), MARZO(Estacion.PRIMAVERA), ABRIL(Estacion.PRIMAVERA),
	MAYO(Estacion.PRIMAVERA), JUNIO(Estacion.VERANO), JULIO(Estacion.VERANO), AGOSTO(Estacion.VERANO),
	SEPTIEMBRE(Estacion.OTO�O), OCTUBRE(Estacion.OTO�O), NOVIEMBRE(Estacion.OTO�O), DICIEMBRE(Estacion.INVIERNO);

	private final Estacion estacion;

	private Mes(Estacion estacion) {
		this.estacion = estacion;
	}

	public Estacion getEstacion() {
		return this.estacion;
	}
}
