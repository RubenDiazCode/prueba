package logistica;

public class Avion extends Transporte {
	@Override
	public void load() {
		System.out.println("CARGANDO AVION");
	}
	
	@Override
	public void save() {
		System.out.println("GUARDANDO EN AVION");
	}
}
