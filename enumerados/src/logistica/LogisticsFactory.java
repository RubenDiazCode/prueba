package logistica;

public class LogisticsFactory {

	public Transporte getInstance(MedioTransporte m) {
		Transporte t=null;
		switch (m) {
		case TIERRA:
			t = new Camion();
			break;
		case AIRE:
			t = new Avion();
			break;
		case MAR:
			t = new Barco();
			break;
		default:

		}
		return t;

	}

}
