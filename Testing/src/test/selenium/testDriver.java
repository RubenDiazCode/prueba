package test.selenium;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class testDriver {

	
	void testChrome() {

		System.setProperty("webdriver.chrome.driver", "webDrivers/chromedriver.exe");
		WebDriver browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.get("https://www.google.com/");
		
		assertTrue(browser.getCurrentUrl().equals("https://www.google.com/"));
		//boton
		WebDriverWait wait = new WebDriverWait(browser,0);
		WebElement btnAceptar=browser.findElement(By.cssSelector("input.gNO89b"));
		
		browser.findElement(By.cssSelector("input.gLFyf.gsfi")).sendKeys("patata");
		wait.until(ExpectedConditions.visibilityOf(btnAceptar));
		btnAceptar.click();
		//screenshot
		captureScreenshot(browser, "screenshots/testChrome","pantallaso1");
		WebElement firstResult = browser.findElement(By.cssSelector("input.gLFyf.gsfi"));
		firstResult.getAttribute("value");
		assertTrue(firstResult.getAttribute("value").equals("patata"));
		browser.quit();
	}
	
	@Test
	void Gmail(){
		System.setProperty("webdriver.chrome.driver", "webDrivers/chromedriver.exe");
		WebDriver browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.get("https://www.google.com/");
		
		assertTrue(browser.getCurrentUrl().equals("https://www.google.com/"));
		//pulsar inicio sesion
		WebDriverWait wait = new WebDriverWait(browser,0);
		WebElement btnInicio=browser.findElement(By.cssSelector("a.gb_ce gb_4 gb_Wc"));
		captureScreenshot(browser, "screenshots/testChrome","pantallasoinicio");
		wait.until(ExpectedConditions.visibilityOf(btnInicio));
		btnInicio.click();
		//meter correo
		
		browser.findElement(By.cssSelector("input.whsOndzHQkbf")).sendKeys("rubendiazee@gmail.com");
		WebElement btnSiguiente=browser.findElement(By.cssSelector("a.gb_ce gb_4 gb_Wc"));
		captureScreenshot(browser, "screenshots/testChrome","pantallasocorreo");
		wait.until(ExpectedConditions.visibilityOf(btnSiguiente));
		btnSiguiente.click();
		
		//meter pass 
		
		//screenshot
		captureScreenshot(browser, "screenshots/testChrome","pantallasoinicio");
		WebElement firstResult = browser.findElement(By.cssSelector("input.gLFyf.gsfi"));
		firstResult.getAttribute("value");
		assertTrue(firstResult.getAttribute("value").equals("patata"));
		browser.quit();
	}

	private static String captureScreenshot(WebDriver driver, String screenshotPath, String screenshotName) {
		String destinationPath = null;
		try {
			File destFolder = new File(screenshotPath);
			destinationPath = destFolder.getCanonicalPath() + "/" + screenshotName + ".png";
			TakesScreenshot screenshot = (TakesScreenshot) driver;
			File sourceFile = screenshot.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(sourceFile, new File(destinationPath));
		} catch (Exception e) {
			System.out.println("Error haciendo captura...\n"+e.getMessage());
			e.printStackTrace();
		}
		return destinationPath;

	}

}
