package com.test.unittesting;

import static org.junit.Assert.*;

import org.junit.Test;

import com.unittesting.Calculadora;

public class CalculadoraTest {

	@Test
	public void multiplicarPorCosas() {
		int resultado = Calculadora.multiplicar(2,2);
		assertEquals(4,resultado);
	}
	
	@Test
	public void sumarCosas() {
		int resultado = Calculadora.sumar(2, 3);
		assertEquals(5,resultado);
	}
	
	@Test
	public void restarCosas() {
		int resultado = Calculadora.restar(1, 4);
		assertEquals(-3,resultado);
	}
	
	@Test
	public void dividirPorCosas() {
		int resultado = Calculadora.dividir(1, 0);
		assertEquals(0,resultado);
		
	}
	@Test
	public void ecuasion() {
		double resultado = Calculadora.segundoGrado(1, 2, 1, 1);
		assertEquals(-1,resultado,.1);
	}


}
