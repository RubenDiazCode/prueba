package com.unittesting;

public class Calculadora {

//	public static int multiplicarPor1(int numero) {
//		return numero * 1;
//	}

//	public static int multiplicarPor2(int numero) {
//		return numero * 2;
//	}

	public static int sumar(int numero1, int numero2) {
		return numero1 + numero2;
	}

	public static int restar(int numero1, int numero2) {
		return numero1 - numero2;
	}

	public static int multiplicar(int numero1, int numero2) {
		return numero1 * numero2;
	}

	public static int dividir(int numero1, int numero2) {

		try {
			if (numero2 == 0)
				throw new Exception();
			return numero1 / numero2;
		} catch (Exception e) {
			// e.printStackTrace();
			return 0;
		}

	}

	public static double segundoGrado(double a, double b, double c, int modo) {
		try {
			if (a == 0)
				throw new Exception();
			double resultado = (-b+Math.sqrt(b*b-4*a*c))/(2*a);
			return resultado * modo;
		} catch (Exception e) {
			return -1;
		}

	}



}
