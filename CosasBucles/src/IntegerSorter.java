import java.util.Scanner;

public class IntegerSorter {

	int[] arrayEnteros;

	public IntegerSorter() {

	}

	public IntegerSorter(int[] arrayEnteros) {
		this.arrayEnteros = arrayEnteros;
	}

	protected void swap(int i, int j) {
		int aux = this.arrayEnteros[j];
		this.arrayEnteros[j] = this.arrayEnteros[i];
		this.arrayEnteros[i] = aux;
	}

	protected void compareAndSwap(int i, int j) {
		if (this.arrayEnteros[j] < this.arrayEnteros[i])
			swap(i, j);
	}

	protected void iteration(int posicion) {
		if (posicion < 0)
			System.out.println("ERROR FATAL, PROCEDO A TIRAR ERROR");
			for(int i=0;i<this.arrayEnteros.length;i++) {
				compareAndSwap(posicion,i);
			}
	}
	
	public void sort() {
		for(int i=0;i<(this.arrayEnteros.length);i++) {
			iteration(i);
		}
	}

	public void imprimir() {
		for (int i = 0; i < this.arrayEnteros.length; i++) {
			System.out.println(this.arrayEnteros[i]);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] listaEnteros = { 5, 6, 4, 8 };
		IntegerSorter i = new IntegerSorter(listaEnteros);
		i.imprimir();
		//i.compareAndSwap(1, 2);
		//i.iteration(3);
		i.sort();
		System.out.println("resultado");
		i.imprimir();
	}

}
