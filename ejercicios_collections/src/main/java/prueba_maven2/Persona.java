package prueba_maven2;

public class Persona {
	private String dni;
	private String nombre;
	private String apellidos;
	private int edad;

	public Persona() {

	}

	public String getNombre() {
		return this.nombre;
	}

	public Persona(String dni, String nombre, String apellidos, int edad) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String apellido1) {
		this.dni = apellido1;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return this.edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return this.nombre + "," + this.dni + "," + this.apellidos + "," + this.edad+"\n";
	}

}
