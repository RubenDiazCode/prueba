package prueba_maven2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ClasePrincipal {
	public static void main(String[] args) {

		try {
			ListaPersona lista = new ListaPersona();
			String opcion ="";
			Scanner sc = new Scanner(System.in);
			while (!opcion.equalsIgnoreCase("5")) {
				System.out.println("Bienvenido al gestor de personas. �Qu� opci�n quieres elegir? ");
				System.out.println("1.Cargar en memoria una lista.");
				System.out.println("2.Guardar la lista en el disco duro.");
				System.out.println("3.Crear una nueva persona");
				System.out.println("4.Imprimir la lista de personas");
				System.out.println("5.Salir");
				System.out.println("Escribe tu selecci�n y pulsa INTRO");

				opcion = sc.nextLine();
				switch (opcion) {
				case "1":
					lista.load();
					System.out.println("Numero de personas:" + lista.getLista().size());
					break;
				case "2":
					lista.save();
					lista.load();
					System.out.println("Numero de personas:" + lista.getLista().size());
					break;

				case "3":
					Persona nuevaPersona=creaPersona();
					lista.add(nuevaPersona);
					break;
				case "4":
					lista.print();
					break;
				case "5":
					System.out.println("Buenas tardes, hasta la pr�xima.");
					break;
				default:
					System.out.println("opci�n inv�lida");

				}
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static Persona creaPersona() {
		Persona p = new Persona();
		Scanner sc=new Scanner(System.in);
		try {
			System.out.println("introduce datos:");
			System.out.println("introduce dni:");
			p.setDni(sc.nextLine());
			System.out.println("introduce nombre:");
			p.setNombre(sc.nextLine());
			System.out.println("introduzca apellidos:");
			p.setApellidos(sc.nextLine());
			System.out.println("introduzca edad:");
			p.setEdad(sc.nextInt());
		}catch(InputMismatchException e) {
			p.setEdad(-1);
		}
		return p;
	}

	public void ejercicioCuatro() {

		// TODO Auto-generated method stub
		try {
			ListaPersona lista = new ListaPersona();
//			Persona p1 = new Persona("1234", "paco", "porras", 22);
//			Persona p2 = new Persona("4321", "ppapa", "pirir", 20);
//			Persona p3 = new Persona("1234", "paco", "sanz", 32);
//			Persona p4 = new Persona("1222", "paco", "porras", 12);
//
//			lista.print();
//			lista.add(p1);
//			lista.add(p2);
//			System.out.println(lista.find("1234").toString());
//			lista.remove(p2);
//			System.out.println(lista.comprobarId(p1));
//			System.out.println(lista.comprobarId(p4));

			lista.load();
			lista.save();
			lista.print();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
