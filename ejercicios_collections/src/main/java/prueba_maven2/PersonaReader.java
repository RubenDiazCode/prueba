package prueba_maven2;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersonaReader implements Serializable {

	private List<Persona> lista;
	File file;

	public PersonaReader(String ruta) {
		this.file = new File(ruta);
		this.lista = leer();

	}

	public List<Persona> getLista() {
		return this.lista;
	}


	public ArrayList<Persona> leer() {
		ArrayList<Persona> listaLeida = new ArrayList<Persona>();
		try {
			Scanner sc = new Scanner(this.file);
			while (sc.hasNextLine()) {
				String pLeida[] = sc.nextLine().split(",");
				Persona p = new Persona(pLeida[0], pLeida[1], pLeida[2], Integer.valueOf(pLeida[3]));
				listaLeida.add(p);

			}
			sc.close();

		} catch (Exception e) {
			System.out.println("YA LA HAS LIADO CON EL FICHERO LEYENDO");
		}
		return listaLeida;
	}

	public static void main(String[] args) {
		PersonaReader pr = new PersonaReader("./ficheroPersonas.csv");
		for(Persona p: pr.lista) {
			System.out.println(p.toString());
		}
		
	}

}
