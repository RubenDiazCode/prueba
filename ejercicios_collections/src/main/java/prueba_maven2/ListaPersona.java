package prueba_maven2;

import java.util.ArrayList;
import java.util.List;

public class ListaPersona {
	private List<Persona> lista;

	public ListaPersona() {
		this.lista = new ArrayList<Persona>();
	}

	public List<Persona> getLista() {
		return this.lista;
	}

	public Persona find(String dni) {
		Persona resultado = null;
		for (Persona p : this.lista) {
			if (p.getDni().equalsIgnoreCase(dni))
				resultado = p;
		}
		return resultado;
	}

	public void add(Persona p) {
		try {
			if (comprobarId(p))
				this.lista.add(p);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void remove(Persona p) {
		try {
			if (comprobarId(p))
				this.lista.remove(p);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean comprobarId(Persona p) throws Exception {
		for (Persona pers : this.lista) {
			if (pers.getDni().equalsIgnoreCase(p.getDni())) {
				throw new Exception("iyo, que hay ya gente con ese id");
			}

		}

		return true;
	}

	public void print() {
		if (this.lista.size() == 0)
			System.out.println("Empty list");
		for (Persona p : this.lista) {
			System.out.println(p.toString());
		}
	}

	public void load() {
		PersonaReader pr = new PersonaReader("./ficheroPersonas.csv");
		pr.leer();
		this.lista = pr.getLista();
	}

	public void save() {
		PersonaWriter pw = new PersonaWriter();
		pw.write(this.lista);
	}

}
