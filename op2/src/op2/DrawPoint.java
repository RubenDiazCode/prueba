package op2;

public class DrawPoint {

	public static void main(String[] args) {
        double x = 0.5;
        double y = 45.64;

        draw(x, y);
        drawLine(x, y, 4.5, 67.54);
    }
    
    private static void draw(double x, double y) {
        System.out.println(String.format("draw point(%.2f, %.2f)", x, y));
    }
    
    private static void drawLine(double x1, double y1, double x2, double y2) {
        System.out.println(String.format("draw line point(%.2f,%.2f) -> point(%.2f,%.2f)", x1, y1, x2, y2));
    }

	
	
}
