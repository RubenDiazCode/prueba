import java.util.Scanner;

public class Persona {

	private String nombre;
	private String apellidos;
	private int edad;
	private Telefono telefono;

	public Persona(String nombre, String apellidos, int edad, Telefono telefono) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.setTelefono(telefono);
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return this.edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String imprimeNombre(String nombre) {
		return "Mi nombre es " + nombre;
	}

	public String imprimeApellidos(String apellidos) {
		return "Mis apellidos es " + apellidos;
	}

	public String imprimeEdad(int edad) {
		return "Mi nombre es " + edad;
	}

	public static Persona creaPersona() {
		Scanner sc=new Scanner(System.in);
		System.out.println("introduce persona:\n");
		String nombre=sc.nextLine();
		String apellidos=sc.nextLine();
		int edad=sc.nextInt();
		System.out.println("introduce telefono de la persona:\n");
		Telefono t=creaTelefono();
		Persona p=new Persona(nombre, apellidos,edad,t);
		return p;
	}
	
	public static void imprimePersona(Persona p1) {
		System.out.println(p1.imprimeNombre(p1.getNombre()));
		System.out.println(p1.imprimeNombre(p1.getApellidos()));
		System.out.println(p1.imprimeEdad(p1.getEdad()));
		Telefono t=p1.getTelefono();
		p1.getTelefono();
		Telefono.imprimeTelefono(t);  
	
	}
	
	public static Telefono creaTelefono() {
		Scanner sc=new Scanner(System.in);
		int numero=sc.nextInt();
		String operador=sc.nextLine();
		String marca=sc.nextLine();
		Telefono t=new Telefono(numero, operador, marca);
		return t;
	}

	public Telefono getTelefono() {
		return this.telefono;
	}

	public void setTelefono(Telefono telefono) {
		this.telefono = telefono;
	}

}
