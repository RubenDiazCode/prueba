
public class Telefono {
	
	private int numero;
	private String operador;
	private String marca;
	
	public Telefono(int numero, String operador, String marca) {
		this.numero=numero;
		this.operador=operador;
		this.marca=marca;
	}
	
	public Telefono(String operador, String marca) {
		this.numero=(int) (Math.random()*999999999);
		this.operador=operador;
		this.marca=marca;
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getOperador() {
		return this.operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	public static void imprimeTelefono(Telefono t) {
		System.out.println(t.imprimeNumero(t.getNumero()));
		System.out.println(t.imprimeOperador(t.getOperador()));
		System.out.println(t.imprimeMarca(t.getMarca()));
	
	}
	
	public String imprimeNumero(int numero) {
		return "Mi numero es " + numero;
	}

	public String imprimeOperador(String operador) {
		return "Mi operador es " + operador;
	}

	public String imprimeMarca(String marca) {
		return "Mi marca es " + marca;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub


	}

}
