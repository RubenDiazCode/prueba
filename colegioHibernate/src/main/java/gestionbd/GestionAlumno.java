package gestionbd;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.Alumno;


public class GestionAlumno {
	private Session session;
	
	public GestionAlumno(Session session) {
		this.session= session;
	}
	
	public List<Alumno> seleccionarTodos(){
		Query<Alumno> query=this.session.createQuery("select * from alumno");
		return query.list();
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session session = utils.HibernateUtils.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			GestionAlumno ga = new GestionAlumno(session);
			List<Alumno> listAlumno=ga.seleccionarTodos();
			for(Alumno a :listAlumno) {
				System.out.println(a.toString());
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
