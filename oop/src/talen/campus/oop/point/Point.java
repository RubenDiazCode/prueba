package talen.campus.oop.point;

public class Point {

	public static void drawLine(Point point1, Point point2) {
		System.out.println(String.format("draw line point(%.2f,%.2f) -> point(%.2f,%.2f)", point1.getX(), point1.getY(), point2.getX(), point2.getY()));
	}

	private double x;

	private double y;
	
	public Point() {
		this.x=0;
		this.y=0;
	}

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public void draw() {
		System.out.println(String.format("draw point(%.2f, %.2f)", this.x, this.y));
	}
	
}
