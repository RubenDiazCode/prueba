package talen.campus.oop.abstracts;

public class Triangulo extends Figura {

    private double base;
    
    private double altura;
	
	public Triangulo(String color, double base , double altura) {
		super(color);
		this.base = base;
		this.altura = altura;
	}

	@Override
	public double calcularArea() {
		return (this.base * this.altura) / 2;
	}

	@Override
	public String toString() {
		return "Triangulo [base=" + this.base + ", altura=" + this.altura + ", color=" + this.color + "]";
	}
}
