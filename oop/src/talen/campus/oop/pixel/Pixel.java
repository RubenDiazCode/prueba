package talen.campus.oop.pixel;

public class Pixel extends Point {

	private String color;
	
	public Pixel() {
		super();
		this.color = "black";
	}
	
	public Pixel(double x, double y) {
		super(x, y);
		this.color = "black";
	}

	public Pixel(double x, double y, String color) {
		super(x, y);
		this.color = color;
	}
	
	//Polimorfismo de inclusión
	
	@Override
	public void draw() {
		super.draw();
		System.out.println(String.format("draw point(%.2f, %.2f) color: %s", this.getX(), this.getY(), this.color));
	}

	
	public static void main(String[] args) {
//		Point point1 = new Point(3, 3);
//		point1.draw();
		
		Pixel pixel = new Pixel();
		pixel.draw();
//		
//		Point point = new Pixel(3, 6, "Rojo");
//		point.draw();
//		
//		Pixel pixel2 = (Pixel) point;
//		pixel2.draw();
	}
	
}
