package talen.campus.oop.interfaces;

public class SimpleLookup implements Lookup {

	private String[] names;
	
	private Object[] values;
	
	public SimpleLookup(String[] names, Object[] values) {
		this.names = names;
		this.values = values;
	}

	@Override
	public Object find(String name) {
		for(int i = 0; i< this.names.length; i++) {
			if(name.equals(this.names[i])) {
				return this.values[i];
			}
		}
		return null;
	}

}
