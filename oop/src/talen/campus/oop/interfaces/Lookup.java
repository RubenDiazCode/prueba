package talen.campus.oop.interfaces;

public interface Lookup {
	Object find(String name);
}
