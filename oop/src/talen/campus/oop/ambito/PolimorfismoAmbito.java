package talen.campus.oop.ambito;

public class PolimorfismoAmbito {

	public static void main(String[] args) {
		new ClaseA().metodo();
		new ClaseB().metodo();
	}

}
