package talent.campus.ejercicio2;

import talent.campus.ejercicio2.factory.LogisticsFactory;
import talent.campus.ejercicio2.factory.TipoEnvio;
import talent.campus.ejercicio2.factory.Transporte;

public class Main {

	public static void main(String[] args) {
		LogisticsFactory factory = new LogisticsFactory();
		Transporte transporte = factory.getInstance(TipoEnvio.BARCO);

		transporte.load();
		transporte.send();
	}

}
