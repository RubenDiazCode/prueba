package talent.campus.ejercicio2.factory;

public abstract class Transporte {
	
	public abstract void send();

	public abstract void load();
}
