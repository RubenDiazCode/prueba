package talent.campus.ejercicio2.factory;

public class Barco extends Transporte {

	protected Barco() {}
	
	@Override
	public void send() {
		System.out.println("Envio por barco");

	}

	@Override
	public void load() {
		System.out.println("Barco cargado");

	}

}
