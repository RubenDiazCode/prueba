package talent.campus.ejercicio2.factory;

public class LogisticsFactory {

	public Transporte getInstance(TipoEnvio tipoEnvio) {
		switch (tipoEnvio) {
		case AVION:
			return new Avion();
			
		case CAMION:
			return new Camion();
			
		case BARCO:
			return new Barco();		
		default:
			throw new RuntimeException("Tipo env�o no definido.");
		}
	}
}
