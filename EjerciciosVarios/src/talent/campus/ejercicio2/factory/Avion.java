package talent.campus.ejercicio2.factory;

public class Avion extends Transporte {

	protected Avion() {}

	@Override
	public void send() {
		System.out.println("Env�o por avi�n");

	}

	@Override
	public void load() {
		System.out.println("Avi�n cargado");

	}

}
