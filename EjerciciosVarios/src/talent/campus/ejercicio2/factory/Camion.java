package talent.campus.ejercicio2.factory;

public class Camion extends Transporte {

	protected Camion() {}

	@Override
	public void send() {
		System.out.println("Envio por tierra");

	}

	@Override
	public void load() {
		System.out.println("Cami�n cargado");

	}

}
