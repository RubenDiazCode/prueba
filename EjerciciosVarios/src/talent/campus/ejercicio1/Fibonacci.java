package talent.campus.ejercicio1;

public class Fibonacci {

	public static final void main(String[] args) {
		new Fibonacci().fibonacci1(8);
	}
	
	public void fibonacci1(int n) {
		int[] fib = new int[n + 1];
		for (int i = 0; i < fib.length; i++) {
			if (i == 0) {
				fib[i] = 0;
			} else if (i == 1) {
				fib[i] = 1;
			} else {
				fib[i] = fib[i - 1] + fib[i - 2];
			}
			
			if (fib[i] % 2 == 0) {
				System.out.println(String.format("fib(%d) = %s", i, fib[i]) + " *");
			} else {
				System.out.println(String.format("fib(%d) = %s", i, fib[i]));
			}
			
		}
	}
	
	public void fibonacci2(int n) {
		int low = 0;
		int high = 1;
		int fib = 0;

		for (int i = 0; i <= n; i++) {
			if (i == 0) {
				System.out.println(String.format("fib(%d) = %s", i, 0) + " *");
			} else if (i == 1) {
				System.out.println(String.format("fib(%d) = %s", i, 1));
			} else {
				fib = low + high;
				high = low + high;
				low = high - low;

				if (fib % 2 == 0) {
					System.out.println(String.format("fib(%d) = %s", i, fib) + " *");
				} else {
					System.out.println(String.format("fib(%d) = %s", i, fib));
				}
			}
		}
	}
}
