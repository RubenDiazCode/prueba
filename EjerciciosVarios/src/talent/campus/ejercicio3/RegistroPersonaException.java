package talent.campus.ejercicio3;

public class RegistroPersonaException extends Exception {

	private static final long serialVersionUID = 7896831169501647677L;

	public RegistroPersonaException(String message) {
		super(message);
	}
}
