package talent.campus.ejercicio3;

public class Persona {
	
	private String dni;
	
	private String nombre;
	
	private String apellidos;

	public Persona(String dni, String nombre, String apellidos) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	public String getDni() {
		return this.dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	@Override
	public String toString() {
		return  this.dni + "-" + this.nombre + " " + this.apellidos;
	}
	
	

}
