package talent.campus.ejercicio3;

public class RegistroPersona {
	
	private static final int NOT_FOUND = -1;
	
	private Persona[] personas;
	
	private int last;

	public RegistroPersona(int n) throws RegistroPersonaException {
		if(n <= 0) {
			throw new RegistroPersonaException("El tama�o de registro debe ser > 0");
		}
		this.personas = new Persona[n];
		this.last = 0;
	}
	
	public void add(Persona persona) throws RegistroPersonaException {
		if(this.last == this.personas.length) {
			throw new RegistroPersonaException("Registro lleno");
		}
		
		this.personas[this.last] = persona;
		this.last++;
	}

	public boolean contains(String dni) throws RegistroPersonaException {
		int pos = this.findLocation(dni);
		return pos != NOT_FOUND;
	}
	
	private int findLocation(String dni) {
		for(int i=0; i < this.personas.length; i++) {
			if(this.personas[i].getDni().equalsIgnoreCase(dni)) {
				return i;
			}
		}
		return NOT_FOUND;
	}

	public Persona find(String dni) throws RegistroPersonaException {
		int pos = this.findLocationWithException(dni);	
		return this.personas[pos];
	}
	
	public Persona remove(String dni) throws RegistroPersonaException {
		int pos = this.findLocationWithException(dni);
		
		Persona persona = this.personas[pos];
		for(int i = pos + 1; i < this.last; i++) {
			this.personas[i - 1] = this.personas[i];
		}
		
		this.last--;
		this.personas[this.last] = null;
		return persona;
	}

	private int findLocationWithException(String dni) throws RegistroPersonaException {
		int pos = this.findLocation(dni);
		if(pos == NOT_FOUND) {
			throw new RegistroPersonaException("La persona con DNI: " + dni + " no existe.");
		}
		return pos;
	}

	public String[] getAll() {
		String[] list = new String[this.last];
		for(int i= 0; i<this.last; i++) {
			list[i] = this.personas[i].toString();
		}
		return list;
	}

}