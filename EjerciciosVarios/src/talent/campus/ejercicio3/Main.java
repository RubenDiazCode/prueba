package talent.campus.ejercicio3;

public class Main {

	public static void main(String[] args) {
		try {
			RegistroPersona reg = new RegistroPersona(5);
			reg.add(new Persona("25437890R", "Nombre1", "Apellidos 1"));
			reg.add(new Persona("X6548906U", "Nombre2", "Apellidos 2"));
			reg.add(new Persona("12345678U", "Nombre3", "Apellidos 3"));
			reg.add(new Persona("00000000T", "Nombre4", "Apellidos 4"));
			reg.add(new Persona("76534213T", "Nombre5", "Apellidos 5"));
			
			printListaPersonas(reg);
			
			System.out.println("Buscar persona");
			Persona per = reg.find("76534213T");
			System.out.println("Persona encontrada: "+per);
			
			System.out.println("Eliminar persona");
			Persona perRemove = reg.remove("12345678U");
			System.out.println("Persona eliminado: "+ perRemove);
			
			printListaPersonas(reg);
			
		} catch (RegistroPersonaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void printListaPersonas(RegistroPersona reg) {
		System.out.println("Lista personas:");
		for(String per : reg.getAll()) {
			System.out.println(per);
		}
	}

}
