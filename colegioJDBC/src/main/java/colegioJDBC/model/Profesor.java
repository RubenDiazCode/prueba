package colegioJDBC.model;

import java.util.HashSet;
import java.util.Set;

public class Profesor {
	private int id;
	private String dni;
	private String nombre;
	private String apellidos;
	private int anyo_nac;
	private int asignatura_id;

	public Profesor() {

	}

	public Profesor(int id, String dni, String nombre, String apellidos, int anyoNac) {
		this.id = id;
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.anyo_nac = anyoNac;

	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getAnyoNac() {
		return this.anyo_nac;
	}

	public void setAnyoNac(int anyoNac) {
		this.anyo_nac = anyoNac;
	}

	public int getAsignaturasProfesor() {
		return this.asignatura_id;
	}

	public void setAsignaturasProfesor(int asignaturasProfesor) {
		this.asignatura_id = asignaturasProfesor;
	}

	@Override
	public String toString() {
		return "Profesor [id=" + this.id + ", dni=" + this.dni + ", nombre=" + this.nombre + ", apellidos="
				+ this.apellidos + ", anyoNac=" + this.anyo_nac + ", asignaturasProfesor=" + this.asignatura_id + "]";
	}

}
