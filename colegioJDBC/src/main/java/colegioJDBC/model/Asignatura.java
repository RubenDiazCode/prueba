package colegioJDBC.model;

public class Asignatura {

	private int id;
	private String nombre;
	private int horasSemanales;
	public Asignatura() {
		
	}
	public Asignatura(int id, String nombre, int horasSemanales) {
		this.id = id;
		this.nombre = nombre;
		this.horasSemanales = horasSemanales;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getHorasSemanales() {
		return this.horasSemanales;
	}
	public void setHorasSemanales(int horasSemanales) {
		this.horasSemanales = horasSemanales;
	}
	@Override
	public String toString() {
		return "Asignatura [id=" + this.id + ", nombre=" + this.nombre + ", horasSemanales=" + this.horasSemanales + "]";
	}
	
	
}
