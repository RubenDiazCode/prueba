package colegioJDBC.model;

public class Alumno {

	private int id;
	private String dni;
	private String nombre;
	private String apellidos;
	private int anyoNac;

	public Alumno() {

	}

	public Alumno(String dni, String nombre, String apellidos, int anyo_nac) {
	
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.anyoNac = anyo_nac;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getAnyo_nac() {
		return this.anyoNac;
	}

	public void setAnyo_nac(int anyo_nac) {
		this.anyoNac = anyo_nac;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.dni+" - "+this.nombre+" - "+this.apellidos+" - "+this.anyoNac;
	}

}
