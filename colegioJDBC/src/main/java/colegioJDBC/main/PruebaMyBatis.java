package colegioJDBC.main;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import colegioJDBC.model.Alumno;
import colegioJDBC.model.Asignatura;
import colegioJDBC.model.Profesor;
import mapper.AlumnoMapper;
import mapper.AsignaturaMapper;
import mapper.ProfesorMapper;
import mapper.AsignaturaAlumnoMapper;

public class PruebaMyBatis {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String resource ="mybatis-config.xml";
		InputStream inputStream=PruebaMyBatis.class.getClassLoader().getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
		
		try(SqlSession session = sqlSessionFactory.openSession()){
			//pruebas de alumno
			AlumnoMapper mapper = session.getMapper(AlumnoMapper.class);
//			mapper.insertAlumno("paca", "antonia", "123224",33);
//			session.commit();
//			List<Alumno> listaAlumno=mapper.selectAlumnoDni("123224");
//			for(Alumno a: listaAlumno) {
//				System.out.println(a.getNombre()+" - "+a.getId());
//			}
		
			//pruebas de asignatura
			AsignaturaMapper asigMapper=session.getMapper(AsignaturaMapper.class);
//			asigMapper.insertAsignatura("mates", 2);
//			
//			asigMapper.insertAsignatura("cono", 7);
//			session.commit();
//			List<Asignatura> listaAsignaturas=asigMapper.selectAsignaturaPorNombre("%co%");
			List<Asignatura> listaAsignaturas=asigMapper.selectAsignaturaPorAlumno("5345253f");
			for(Asignatura a: listaAsignaturas) {
				System.out.println(a.toString());
			}
			
			
			//pruebas de profesor
			ProfesorMapper profeMapper=session.getMapper(ProfesorMapper.class);
//			profeMapper.insertProfesor("1315244F","pocoyo","gimienez" ,1977 , 1);
//			profeMapper.insertProfesor("3532523F","pacaya","jimienez" ,1977 , 2);
//			profeMapper.insertProfesor("446464s", "jesus", "jesus", 1991);
//			profeMapper.asignarAsignatura(2,"jesus");
//			session.commit();
			
			List<Profesor> listaProfes = profeMapper.selectProfePorEdad(22);
			for(Profesor p: listaProfes) {
				System.out.println(p.toString());
			}
			
			//pruebas de transicion
//			AsignaturaAlumnoMapper transicionMapper=session.getMapper(AsignaturaAlumnoMapper.class);
//			transicionMapper.insertAlumnoPorAsignatura(1, 2);
//			session.commit();
//						
		}
	}

}
