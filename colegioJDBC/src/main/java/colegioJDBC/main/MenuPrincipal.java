package colegioJDBC.main;

import java.util.Scanner;

import colegioJDBC.model.Alumno;
import service.ConectionMyBatis;
import service.OperacionesMyBatis;

public class MenuPrincipal {

	public static void main(String[] args) {
		System.out.println("Bienvenido al supermen�, elige una opci�n por n�mero");
		OperacionesMyBatis o = new OperacionesMyBatis();
		Scanner sc = new Scanner(System.in);
		String opcion = "";
		// gran display de println de calidad
		System.out.println("1-crear una asignatura");
		System.out.println("2-crear un alumno");
		System.out.println("3-crear un profesor");
		System.out.println("4-listado de asignaturas");
		System.out.println("5-listado de alumnos");
		System.out.println("6-listado de profesores");
		System.out.println("7-buscar asignatura por nombre");
		System.out.println("8-buscar alumno por dni");
		System.out.println("9-buscar profesores por edad");
		System.out.println("10-vincular un profesor a una asignatura");
		System.out.println("11-vincular alumno a asignatura");
		System.out.println("12-consultar asignaturas de alumno");
		System.out.println("13-salir");

		while (!opcion.equalsIgnoreCase("13")) {
			opcion = sc.nextLine();

			switch (opcion) {
			case "1":
				System.out.println("inserta nombre:");
				String nombre = sc.nextLine();
				System.out.println("inserta horas:");
				int horas = sc.nextInt();
				System.out.println("asignatura insertada " + o.insertarAsignatura(nombre, horas));
				break;
			case "2":
				Alumno a = new Alumno();
				System.out.println("inserta nombre:");
				a.setNombre(sc.nextLine());
				System.out.println("inserta dni");
				a.setDni(sc.nextLine());
				System.out.println("inserta apellidos");
				a.setApellidos(sc.nextLine());
				System.out.println("inserta año de nacimiento");
				a.setAnyo_nac(sc.nextInt());
				System.out.println("alumno insertado " + o.insertarAlumno(a));

				break;
			case "3":
				System.out.println("inserta nombre:");
				String nombreProfe = sc.nextLine();
				System.out.println("inserta dni");
				String dniProfe = sc.nextLine();
				System.out.println("inserta apellidos");
				String apellidosProfe = sc.nextLine();
				System.out.println("inserta año de nacimiento");
				int anyoNacProfe = sc.nextInt();
				System.out.println("Profesor insertado "
						+ o.insertarProfesor(anyoNacProfe, dniProfe, nombreProfe, apellidosProfe));
				break;
			case "4":
				System.out.println(o.devolverAsignaturas());
				break;
			case "5":
				System.out.println(o.devolverAlumnos());
				break;
			case "6":
				System.out.println(o.devolverProfesores());
				break;
			case "7":
				System.out.println("introduce nombre de asignatura");
				String nombreAsig = sc.nextLine();
				System.out.println(o.asignaturasPorNombre(nombreAsig));
				break;
			case "8":
				System.out.println("introduce dni del alumno");
				String dniAlumno = sc.nextLine();
				System.out.println(o.alumnoPorDNI(dniAlumno));
				break;
			case "9":
				System.out.println("introduce edad que quieras buscar");
				int edadProfe = sc.nextInt();
				System.out.println("profesores mayores de " + edadProfe + " años:\n" + o.profesoresPorEdad(edadProfe));
				break;
			case "10":
				System.out.println("introduce nombre del profesor:");
				String nombreProfesor = sc.nextLine();
				System.out.println("introduce id de la asignatura: ");
				int idAsignatura = sc.nextInt();
				System.out.println("la asignatura ha sido asignada al profesor "
						+ o.asignarAsignatura(idAsignatura, nombreProfesor));
				break;
			case "11":
				System.out.println("introduce id del alumno");
				int idAlumno = sc.nextInt();
				System.out.println("introduce la id de la asignatura");
				int idAsig = sc.nextInt();
				System.out.println("alumno vinculado a asignatura " + o.vincularAlumno(idAsig, idAlumno));
				break;
			case "12":
				System.out.println("introduce dni de alumno");
				String dniParaBuscar = sc.nextLine();
				System.out.println("asignaturas:\n" + o.asignaturasPorAlumno(dniParaBuscar));
				break;
			case "13":
				System.out.println("Enga hasta luego");
				ConectionMyBatis.cerrarConexion(o.getSession());
				break;
			default:
				System.out.println("opcion incorrecta");
				break;
			}
		}

	}

}
