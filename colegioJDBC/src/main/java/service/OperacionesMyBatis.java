package service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import colegioJDBC.model.Alumno;
import colegioJDBC.model.Asignatura;
import colegioJDBC.model.Profesor;
import mapper.AlumnoMapper;
import mapper.AsignaturaAlumnoMapper;
import mapper.AsignaturaMapper;
import mapper.ProfesorMapper;

public class OperacionesMyBatis {
	SqlSession session;

	public OperacionesMyBatis() {
		this.session = ConectionMyBatis.obtenerConexion();
	}

	public SqlSession getSession() {
		return this.session;
	}

	// alumnos
	public String devolverAlumnos() {
		AlumnoMapper mapper = this.session.getMapper(AlumnoMapper.class);
		List<Alumno> listaAlumno = mapper.selectAllAlumnos();
		String salida = "";
		for (Alumno a : listaAlumno) {
			salida += a.toString() + "\n";
		}
		return salida;
	}

	public boolean insertarAlumno(Alumno a) {
		AlumnoMapper mapper = this.session.getMapper(AlumnoMapper.class);
		boolean resultado = mapper.insertAlumno(a.getNombre(), a.getApellidos(), a.getDni(), a.getAnyo_nac());
		if (resultado)
			this.session.commit();
		return resultado;
	}

	public String alumnoPorDNI(String dni) {
		AlumnoMapper mapper = this.session.getMapper(AlumnoMapper.class);
		String resultado = mapper.selectAlumnoDni(dni).toString();
		return resultado;
	}

	// asignaturas
	public String devolverAsignaturas() {
		AsignaturaMapper mapper = this.session.getMapper(AsignaturaMapper.class);
		List<Asignatura> lista = mapper.selectAllAsignaturas();
		String salida = "";
		for (Asignatura a : lista) {
			salida += a.toString() + "\n";
		}
		return salida;
	}

	public boolean insertarAsignatura(String nombre, int horas) {
		AsignaturaMapper mapper = this.session.getMapper(AsignaturaMapper.class);
		boolean resultado = mapper.insertAsignatura(nombre, horas);
		if (resultado)
			this.session.commit();
		return resultado;
	}

	public String asignaturasPorNombre(String nombre) {
		AsignaturaMapper mapper = this.session.getMapper(AsignaturaMapper.class);
		List<Asignatura> lista = mapper.selectAsignaturaPorNombre("%" + nombre + "%");
		String salida = "";
		for (Asignatura a : lista) {
			salida += a.toString() + "\n";
		}
		return salida;
	}

	public String asignaturasPorAlumno(String dni) {
		AsignaturaMapper mapper = this.session.getMapper(AsignaturaMapper.class);
		List<Asignatura> lista = mapper.selectAsignaturaPorAlumno(dni);
		String salida = "";
		for (Asignatura a : lista) {
			salida += a.toString() + "\n";
		}
		return salida;
	}

//profesores

	public String devolverProfesores() {
		ProfesorMapper mapper = this.session.getMapper(ProfesorMapper.class);
		List<Profesor> lista = mapper.selectAllProfesores();
		String salida = "";
		for (Profesor p : lista) {
			salida += p.toString() + "\n";
		}
		return salida;
	}

	public boolean insertarProfesor(int anyoNac, String dni, String nombre, String apellidos) {
		ProfesorMapper mapper = this.session.getMapper(ProfesorMapper.class);
		boolean resultado = mapper.insertProfesor(dni, nombre, apellidos, anyoNac);
		if (resultado)
			this.session.commit();
		return resultado;

	}

	public String profesoresPorEdad(int edad) {
		ProfesorMapper mapper = this.session.getMapper(ProfesorMapper.class);
		List<Profesor> lista = mapper.selectProfePorEdad(edad);
		String salida = "";
		for (Profesor p : lista) {
			salida += p.toString() + "\n";
		}
		return salida;
	}

	public boolean asignarAsignatura(int id, String nombre) {
		ProfesorMapper mapper = this.session.getMapper(ProfesorMapper.class);
		boolean resultado = mapper.asignarAsignatura(id, nombre);
		if (resultado)
			this.session.commit();
		return resultado;
	}

//tabla de transicion
	public boolean vincularAlumno(int idAsignatura, int idAlumno) {
		AsignaturaAlumnoMapper mapper = this.session.getMapper(AsignaturaAlumnoMapper.class);
		boolean resultado = mapper.insertAlumnoPorAsignatura(idAsignatura, idAlumno);
		if (resultado)
			this.session.commit();
		return resultado;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		OperacionesMyBatis o = new OperacionesMyBatis();
		o.insertarAsignatura("valenciano", 10);
		System.out.println(o.devolverAsignaturas());

	}

}
