package service;

import java.io.InputStream;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class ConectionMyBatis {

	
	private static SqlSession session =null;
	
	private static String resource;
	private static InputStream inputStream;
	private static SqlSessionFactory sqlSessionFactory;
	
	public static void conexion() {
		resource="mybatis-config.xml";
		inputStream=ConectionMyBatis.class.getClassLoader().getResourceAsStream(resource);
		sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);
		
		try {
			session =sqlSessionFactory.openSession();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static SqlSession obtenerConexion() {
		if(session==null) {
			conexion();
		}
		return session;
	}
	
	public static void cerrarConexion(SqlSession session) {
			session.close();
	}
}
