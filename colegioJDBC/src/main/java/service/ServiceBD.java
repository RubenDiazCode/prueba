package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ServiceBD {
	private final String DB_URL = "jdbc:mysql://localhost/colegio_jdbc?serverTimezone=UTC";
	private final String USER = "root";
	private final String PASS = "Talent0";

	private Connection conn;
	private Statement stmt;

	public ServiceBD() {
		try {
			this.conn = DriverManager.getConnection(this.DB_URL, this.USER, this.PASS);
			this.stmt=this.conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

	public void randomSelect(String sql) {
    	try(
    			ResultSet rs=this.stmt.executeQuery(sql)){
    		while(rs.next()) {
    			int id=rs.getInt("id");
    			String nombre=rs.getString("nombre");
    			System.out.println("id "+id);
    			System.out.println("nombre "+nombre);
    		}
    		
    	} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
	
	public void randomUpdate(String sql) {
		try { 		
			int rs =this.stmt.executeUpdate(sql);
			
			System.out.println("nº columnas afectadas: "+rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {
//		Class.forName("com.mysql.cj.jdbc.Driver");
		ServiceBD s=new ServiceBD();
		String sql = "SELECT id, nombre FROM alumno";
		s.randomSelect(sql);
		String update="insert into alumno (nombre,dni,apellidos,anyo_nac) "+
		"values ('antonio','536488f','jamon',1998)";
		//s.randomUpdate(update);


	}

}
