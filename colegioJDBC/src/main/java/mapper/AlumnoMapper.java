package mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import colegioJDBC.model.Alumno;

public interface AlumnoMapper {
	public Alumno selectAlumno(int id);

	@Select("select * from alumno")
	public List<Alumno> selectAllAlumnos();

	@Select("select * from alumno where dni = #{param1}")
	public Alumno selectAlumnoDni(String dni);

	@Insert("insert into Alumno (nombre, apellidos, dni, anyo_nac) values (#{param1}, #{param2}, #{param3}, #{param4})")
	public boolean insertAlumno(String nombre, String apellidos, String dni, int anyo_nac);

}
