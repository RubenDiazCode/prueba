package mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import colegioJDBC.model.Asignatura;

public interface AsignaturaMapper {

	public Asignatura selectAsignatura(int id);

	@Select("select * from asignatura")
	public List<Asignatura> selectAllAsignaturas();
	
	@Select("select * from asignatura where nombre like #{param1}") //el % se pasa en la string por parametro
	public List<Asignatura> selectAsignaturaPorNombre(String nombre);
	
	@Select("select a.nombre, a.horas_semanales " + 
			"from asignatura a " + 
			"inner join asignaturaalumno aa on a.id = aa.Asignatura_id " + 
			"inner join alumno al on aa.Alumno_id " + 
			"where al.dni = #{param1}")
	public List<Asignatura>selectAsignaturaPorAlumno(String dni);
	
	
	
	@Insert("insert into asignatura (nombre, horas_semanales) values"+
			"(#{param1}, #{param2})")
	public boolean insertAsignatura(String nombre, int horas_semanales);

}
