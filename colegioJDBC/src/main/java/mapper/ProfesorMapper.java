package mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import colegioJDBC.model.Profesor;

public interface ProfesorMapper {
	public Profesor selectProfesor(int id);

	@Select("select * from Profesor")
	public List<Profesor> selectAllProfesores();
	
	@Select("select * from Profesor where year(SYSDATE()) - anyo_nac >  #{param1}")
	public List<Profesor> selectProfePorEdad(int edad);

	@Insert("insert into Profesor (dni, nombre, apellidos, anyo_nac)"
			+ "values(#{param1}, #{param2}, #{param3}, #{param4})")
	public boolean insertProfesor(String dni, String nombre, String apellidos, int anyo_nac);
	
	@Update("update profesor set asignatura_id = #{param1} where nombre = #{param2}")
	public boolean asignarAsignatura(int asignatura_id, String nombre);

}