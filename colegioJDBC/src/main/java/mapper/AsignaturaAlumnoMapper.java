package mapper;

import org.apache.ibatis.annotations.Insert;

public interface AsignaturaAlumnoMapper {
	@Insert("insert into asignaturaalumno (Asignatura_id, Alumno_id)"
			+ "values(#{param1}, #{param2})")
	public boolean insertAlumnoPorAsignatura(int idAsignatura, int idAlumno);

}
