package talent.campus;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = "/WelcomeForward")
public class WelcomeForward extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		String inputName = req.getParameter("first_name");
		String inputLastname = req.getParameter("last_name");

		String title = "Welcome Forward!!!";
		
		PrintWriter out = resp.getWriter();
		out.println("<!doctype html public \\\"-//w3c//dtd html 4.0 transitional//en\\\"><html><head><title>" + title + "</title></head>" + "<body bgcolor = \"#f0f0f0\">\n"
				+ "<h1 align = \"center\">" + title + "</h1><ul>" + "  <li><b>First Name</b>: " + inputName
				+ "<li><b>Last Name</b>: " + inputLastname + "</ul></body></html>");
	}
}
