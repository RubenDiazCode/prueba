package talent.campus;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = "/HelloForm", initParams = { @WebInitParam(name = "init_first_name", value = "empty"),
		@WebInitParam(name = "init_last_name", value = "empty")

})
public class HelloForm extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		String title = "Using POST Method to Read Form Data";
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n";

		String inputName = request.getParameter("first_name");
		String inputLastname = request.getParameter("last_name");

		if ("forward".equals(inputName)) {
			RequestDispatcher rdObj = request.getRequestDispatcher("/WelcomeForward");
			rdObj.forward(request, response);
		}else if ("root".equals(inputName)) {
			RequestDispatcher rdObj = request.getRequestDispatcher("/admin.html");

			out.println(docType + "<html>\n" + "<head><title>" + title + "</title></head>\n"
					+ "<body bgcolor = \"#f0f0f0\">\n" + "<h1 align = \"center\">" + title + "</h1>\n");

			// incluye el contenido de /admin.html en mi respuesta
			rdObj.include(request, response);

			out.println("<ul><li><b>First Name</b>: " + inputName + "\n" + "  <li><b>Last Name</b>: " + inputLastname
					+ "</ul></body></html>");
		} else {
			Optional<Cookie> optional = Arrays.stream(request.getCookies())
					.filter(cookie -> cookie.getName().equals("full_name"))
					.findFirst();
			if(! optional.isPresent()) {
				Cookie firstName = new Cookie("full_name", inputName + "_" + inputLastname);
				firstName.setMaxAge(60);
				response.addCookie( firstName );				
			}
			
			out.println(docType + "<html><head><title>" + title + "</title></head>" + "<body bgcolor = \"#f0f0f0\">\n"
					+ "<h1 align = \"center\">" + title + "</h1><ul>" + "  <li><b>First Name</b>: " + inputName
					+ "<li><b>Last Name</b>: " + inputLastname + "</ul></body></html>");
		}

	}
}