package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "profesor")
public class Profesor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name ="nombre")
	private String nombre;
	
	@Column(name = "apellidos")
	private String apellidos;
	
	@Column(name = "edad")
	private Integer edad;
	
	@Column(name= "sexo")
	private String sexo;
	
	@Column(name= "dni")
	private String dni;
	
	//relacion con profesores
	@ManyToMany
	@JoinTable(name = "profesor_asignatura",
	joinColumns = @JoinColumn(name = "Profesor_id"),
	inverseJoinColumns = @JoinColumn(name = "Asignatura_id"))
	private List<Asignatura> asignaturas = new ArrayList<Asignatura>();
	
	public Profesor () {
		
	}
	
	

	public Integer getId() {
		return this.id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getNombre() {
		return this.nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getApellidos() {
		return this.apellidos;
	}



	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}



	public Integer getEdad() {
		return this.edad;
	}



	public void setEdad(Integer edad) {
		this.edad = edad;
	}



	public String getSexo() {
		return this.sexo;
	}



	public void setSexo(String sexo) {
		this.sexo = sexo;
	}



	public String getDni() {
		return this.dni;
	}



	public void setDni(String dni) {
		this.dni = dni;
	}



	public List<Asignatura> getAsignaturas() {
		return this.asignaturas;
	}



	public void setAsignaturas(List<Asignatura> asignaturas) {
		this.asignaturas = asignaturas;
	}



	@Override
	public String toString() {
		return "Profesor [id=" + this.id + ", nombre=" + this.nombre +
				", apellidos=" + this.apellidos + ", edad=" + this.edad + ", sexo="
				+ this.sexo + ", dni=" + this.dni + "]";
	}
	
	
	
}
