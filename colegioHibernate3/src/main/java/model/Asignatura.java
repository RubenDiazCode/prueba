package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "asignatura")

public class Asignatura {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "siglas")
	private String siglas;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "horas_lectivas")
	private Integer horasLectivas;
	
	@ManyToMany(mappedBy = "asignaturas") //relacion n:m con profesores
	private List<Profesor> profesores = new ArrayList<Profesor>();
	

	public Asignatura() {

	}
	
	

	public Integer getId() {
		return this.id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getSiglas() {
		return this.siglas;
	}



	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}



	public String getNombre() {
		return this.nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Integer getHorasLectivas() {
		return this.horasLectivas;
	}



	public void setHorasLectivas(Integer horasLectivas) {
		this.horasLectivas = horasLectivas;
	}



	public List<Profesor> getProfesores() {
		return this.profesores;
	}



	public void setProfesores(List<Profesor> profesores) {
		this.profesores = profesores;
	}



	@Override
	public String toString() {
		return "Asignatura [id=" + this.id + ", siglas=" + this.siglas + ", nombre=" + this.nombre + ", horasLectivas="
				+ this.horasLectivas + "]";
	}

}
