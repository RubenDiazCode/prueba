package main;

import org.hibernate.Session;

import utils.HibernateUtils;

public class Main {
	public static void main(String[] args) {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.getTransaction().commit();
//			System.out.println(p.toString());

			// session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
			HibernateUtils.shutdown();
		}
	}
}
