package talent.campus.ejercicio1;

public class ValidadorCodigoPostal {

	public void validarCodigoPostal(String _codigoPostal) throws CodigoPostalException{
		if(_codigoPostal == null) {
			throw new CodigoPostalException("El código postal es nulo.");
		}
		String codigoPostal = _codigoPostal.trim();
		if(codigoPostal.length() != 5) {
			throw new CodigoPostalException("El código postal debe tener 5 digitos.");
		}
		
		for(int i = 0; i<codigoPostal.length(); i++) {
			if(! Character.isDigit(codigoPostal.charAt(i))) {
				throw new CodigoPostalException("El código postal solo puede estar formado por dígitos.");
			}
		}
	}
}
