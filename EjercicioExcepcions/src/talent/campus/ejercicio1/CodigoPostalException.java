package talent.campus.ejercicio1;

public class CodigoPostalException extends Exception {

	private static final long serialVersionUID = -5843403017961663638L;

	public CodigoPostalException(String message) {
		super(message);
	}


}
