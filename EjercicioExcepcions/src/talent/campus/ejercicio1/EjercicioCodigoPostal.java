package talent.campus.ejercicio1;

import java.util.Scanner;

public class EjercicioCodigoPostal {
	
	
	public static void main(String[] args) {
		System.out.println("Introduzca un código postal.");
		Scanner scanner = new Scanner(System.in);
		String codigo = scanner.nextLine();
		
		System.out.println("Codigo postal introducido." + codigo);
		ValidadorCodigoPostal validadorCodigoPostal = new ValidadorCodigoPostal();
		
		try {
			validadorCodigoPostal.validarCodigoPostal(codigo);
		} catch (CodigoPostalException e) {
			System.err.println(e.getMessage());
		}
				
		scanner.close();
	}

}
