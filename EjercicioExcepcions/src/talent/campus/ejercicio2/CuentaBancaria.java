package talent.campus.ejercicio2;

public class CuentaBancaria {

	private String numeroCuenta;
	
	private String titular;
	
	private double saldo;

	public CuentaBancaria(String numeroCuenta, String titular, double saldo) {
		this.numeroCuenta = numeroCuenta;
		this.titular = titular;
		this.saldo = saldo;
	}

	public String getNumeroCuenta() {
		return this.numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTitular() {
		return this.titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return "CuentaBancaria [numeroCuenta=" + this.numeroCuenta + ", titular=" + this.titular + ", saldo=" + this.saldo + "]";
	}

}
