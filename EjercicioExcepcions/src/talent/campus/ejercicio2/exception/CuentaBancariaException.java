package talent.campus.ejercicio2.exception;

public class CuentaBancariaException extends Exception {

	private static final long serialVersionUID = 5616063693042034823L;

	public CuentaBancariaException(String message) {
		super(message);
	}

}
