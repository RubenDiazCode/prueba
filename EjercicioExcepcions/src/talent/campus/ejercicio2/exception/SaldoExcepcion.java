package talent.campus.ejercicio2.exception;

public class SaldoExcepcion extends CuentaBancariaException {

	private static final long serialVersionUID = -6504496827290159948L;

	public SaldoExcepcion(String message) {
		super(message);
	}

}
