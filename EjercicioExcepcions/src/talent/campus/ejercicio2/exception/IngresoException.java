package talent.campus.ejercicio2.exception;

public class IngresoException extends CuentaBancariaException {

	private static final long serialVersionUID = -2349401863225547042L;

	public IngresoException(String message) {
		super(message);
	}

}
