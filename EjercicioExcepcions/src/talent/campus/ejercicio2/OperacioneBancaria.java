package talent.campus.ejercicio2;

import talent.campus.ejercicio2.exception.IngresoException;
import talent.campus.ejercicio2.exception.SaldoExcepcion;

public class OperacioneBancaria {

	private static final double IMPORTE_MAXIMO_RETIRAR = 500.0;
	
	private static final double IMPORTE_MAXIMO_INGRESAR = 1000.0;

	public void ingresar(CuentaBancaria cuentaBancaria, double importe) throws IngresoException {
		if(importe <= 0.0 ) {
			throw new IngresoException("El importe debe ser > 0");
		}
		
		if(importe > IMPORTE_MAXIMO_INGRESAR ) {
			throw new IngresoException("El importe que se ingresar es mayor del maxímo admitido por una singula operación");
		}
		
		cuentaBancaria.setSaldo(cuentaBancaria.getSaldo() + importe);
	}
	
	public void retirar(CuentaBancaria cuentaBancaria, double importe) throws SaldoExcepcion {
		if(importe <= 0.0 ) {
			throw new SaldoExcepcion("El importe debe ser > 0");
		}
		
		if(importe > IMPORTE_MAXIMO_RETIRAR ) {
			throw new SaldoExcepcion("El importe que se quiere retirar es mayor del maxímo admitido por una singula operación");
		}
		
		if(importe > cuentaBancaria.getSaldo()) {
			throw new SaldoExcepcion("El importe que se quiere retirar es mayor del saldo disponible");
		}
		
		cuentaBancaria.setSaldo(cuentaBancaria.getSaldo() - importe);		
	}
	

}
