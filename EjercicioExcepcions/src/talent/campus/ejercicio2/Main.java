package talent.campus.ejercicio2;

import java.util.Scanner;

import talent.campus.ejercicio2.exception.IngresoException;
import talent.campus.ejercicio2.exception.SaldoExcepcion;

public class Main {

	public static void main(String[] args) {
		
		CuentaBancaria cuentaBancaria = new CuentaBancaria("ES2730585530330865606216", "Anelida Camazon Dineros", 500);
		OperacioneBancaria operacioneBancaria = new OperacioneBancaria();
		Scanner scanner = new Scanner(System.in);
		
		String operacion = "";
		while( ! "salir".equalsIgnoreCase(operacion) ) {
			System.out.println("Introduzca ingresar ,retirar, salir");
			
			operacion = scanner.nextLine().trim();
			if("ingresar".equalsIgnoreCase(operacion)) {
				ingresarDinero(cuentaBancaria, operacioneBancaria, scanner);				
			}			
			
			if("retirar".equalsIgnoreCase(operacion)) {
				retirarDinero(cuentaBancaria, operacioneBancaria, scanner);
			}
			
			System.out.println(cuentaBancaria.toString());
		}
		
		scanner.close();
		System.out.println("Saliendo del programa");
	}

	private static void ingresarDinero(CuentaBancaria cuentaBancaria, OperacioneBancaria operacioneBancaria,
			Scanner scanner) {
		boolean correcto = true;
		do {
			correcto = true;
			double importe =  leerImporte(scanner,"Introduzca cantidad a ingresar (<= 1000)" );
			try {
				operacioneBancaria.ingresar(cuentaBancaria, importe);
			} catch (IngresoException e) {
				System.err.println(e.getMessage());
				correcto = false;
			}			
		} while(! correcto);

	}

	private static void retirarDinero(CuentaBancaria cuentaBancaria, OperacioneBancaria operacioneBancaria,
			Scanner scanner) {
		
		boolean correcto = true;
		do {
			correcto = true;
			double importe = leerImporte(scanner, "Introduzca cantidad a retirar (<= 500)");
			try {
				operacioneBancaria.retirar(cuentaBancaria, importe);
			} catch (SaldoExcepcion e) {
				System.err.println(e.getMessage());
				correcto = false;
			}			
		} while(! correcto);
	}

	private static double leerImporte(Scanner scanner, String texto) {
		System.out.println(texto);
		double importe = scanner.nextDouble();
		scanner.nextLine();
		return importe;
	}

}
