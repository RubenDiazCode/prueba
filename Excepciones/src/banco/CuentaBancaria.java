package banco;

import java.util.Scanner;

public class CuentaBancaria {
	int numeroCuenta;
	int saldo;
	String titular;

	public CuentaBancaria() {

	}

	public CuentaBancaria(int numeroCuenta, int saldo, String titular) {
		this.numeroCuenta = numeroCuenta;
		this.saldo = saldo;
		this.titular = titular;
	}

	public void anyadirSaldo() throws ExcepcionDinero {
		Scanner sc = new Scanner(System.in);
		System.out.println("Cuanto dinero?");
		int dinero = sc.nextInt();
		comprobarNegativo(dinero);
		if (dinero > 1000)
			throw new ExcepcionDinero("no se puede meter mas de 1000");
		this.saldo += dinero;

	}

	public void retirarSaldo() throws ExcepcionDinero {
		Scanner sc = new Scanner(System.in);
		System.out.println("Cuanto dinero?");
		int dinero = sc.nextInt();
		comprobarNegativo(dinero);
		if (dinero > 500)
			throw new ExcepcionDinero("no puedes retirar mas de 500");
		this.saldo -= dinero;
		comprobarNegativo(this.saldo);
	}

	public void comprobarNegativo(int dinero) throws ExcepcionDinero {
		if (dinero <= 0)
			throw new ExcepcionDinero("numero negativo");
	}

}
