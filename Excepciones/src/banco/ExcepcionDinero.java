package banco;

public class ExcepcionDinero extends Exception{
public ExcepcionDinero() {
	
}
public ExcepcionDinero(String message) {
	super(message);
}

public ExcepcionDinero(String message, Throwable cause) {
	super(message,cause);
}
}
