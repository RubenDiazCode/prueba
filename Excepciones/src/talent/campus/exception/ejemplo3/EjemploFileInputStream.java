package talent.campus.exception.ejemplo3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class EjemploFileInputStream {

	public static final void main(String[] args) {

		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream("/tmp/tmp.txt");

			byte block[] = new byte[1024];
			while (inputStream.read(block) != -1) {
				System.out.print(new String(block));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
