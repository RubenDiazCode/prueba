package talent.campus.exception.ejemplo2;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Introduzca un numero: 0 para MyException");

		try {
			int numero = readInteger(scanner);
			System.out.println("Has introducido: " + numero);
		} catch (MyException e) {
			System.err.println("Error!! Msg:" + e.getMessage());
			e.printStackTrace();
		}
	}

	private static int readInteger(Scanner scanner) throws MyException {
		int numero = scanner.nextInt();
		if(numero == 0) {
			throw new MyException("No se puede introducir 0","mensaje adicional");
		}
		return numero;
	}

}
