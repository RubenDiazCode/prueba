package talent.campus.exception.ejemplo2;

public class MyException extends Exception {
	
	private String detalles;

	public MyException() {
		// MyException
	}

	public MyException(String message, String detalles) {
		super(message);
		this.detalles="detalles";
	}

	public MyException(Throwable cause) {
		super(cause);
	}

	public MyException(String message, Throwable cause) {
		super(message, cause);
	}

}
