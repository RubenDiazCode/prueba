package talent.campus.exception.ejemplo1;

public class PruebasExcepciones {

	public void paso1(int numero) throws ArithmeticException {
		System.out.println("Estoy en el paso 1");
		this.paso2(numero);
	}

	public void paso2(int numero) throws ArithmeticException {
		System.out.println("Estoy en el paso 2");
		this.paso3(numero);
	}

	public void paso3(int numero) throws ArithmeticException {
		System.out.println("Estoy en el paso 3");
		this.paso4(numero);
	}

	public void paso4(int numero) throws ArithmeticException {
		System.out.println("Estoy en el paso 4");
		this.paso5(numero);
	}

	public void paso5(int numero) throws ArithmeticException {
		System.out.println("Estoy en el paso 5");
		System.out.println("10 entre " + numero + " es: " + 10 / numero);
	}
}
