package talent.campus.exception.ejemplo1;

public class App {

	public static void main(String[] args) {
		PruebasExcepciones prueba = new PruebasExcepciones();
		try {
			prueba.paso1(0);
		} catch (Exception e) {
			System.out.println("Tratando la excepción");
		}
	}

}
