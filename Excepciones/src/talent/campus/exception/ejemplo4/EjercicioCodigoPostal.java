package talent.campus.exception.ejemplo4;

import java.util.Scanner;

public class EjercicioCodigoPostal {
	
	private static void validarCodigoPostal(String _codigoPostal) throws ExcepcionMal {
		if(_codigoPostal == null) {
			throw new ExcepcionMal("no has metido nada capulli");
			
		}
		String codigoPostal = _codigoPostal.trim();
		if(codigoPostal.length() != 5) {
			throw new ExcepcionMal("el codigo postal es demasiado largo");
		}
		
		for(int i = 0; i<codigoPostal.length(); i++) {
			if(! Character.isDigit(codigoPostal.charAt(i))) {
				throw new ExcepcionMal("has metido caracteres capulli");
			}
		}
		
	}
	
	public static void main(String[] args) throws ExcepcionMal {
		
		System.out.println("Introduzca un c�digo postal.");
		Scanner scanner = new Scanner(System.in);
		String codigo = scanner.nextLine();
		
		System.out.println("Codigo postal introducido." + codigo);
		validarCodigoPostal(codigo);
//		if( new EjercicioCodigoPostal().validarCodigoPostal(codigo)) {
//			System.out.println("Codigo postal OK.");
//		} else {
//			System.err.println("Codigo postal KO.");
//		}
		
		scanner.close();
	}

}
