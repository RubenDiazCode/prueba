package talent.campus.exception.ejemplo4;

public class ExcepcionMal extends Exception{
	
	public ExcepcionMal() {
		
	}
	public ExcepcionMal(String message) {
		super(message);
	}

	public ExcepcionMal(Throwable cause) {
		super(cause);
	}

	public ExcepcionMal(String message, Throwable cause) {
		super(message, cause);

	}
}
