package colegio.domain;

public abstract class Persona {
	private String dni;
	private String nombre;
	private String apellidos;
	private String sexo;
	private int edad;

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public int getEdad() {
		return this.edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.dni+" - "+this.nombre+" - "+this.apellidos+" - "+this.sexo+" - "+
				this.edad;
	}

}
