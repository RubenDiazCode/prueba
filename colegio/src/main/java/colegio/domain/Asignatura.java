package colegio.domain;

public class Asignatura {

	private String sigla;
	private String nombre;
	private int horasLectivas;
	
	public Asignatura() {
		
	}
	
	public String getSiglas() {
		return this.sigla;
	}
	public void setSiglas(String siglas) {
		this.sigla = siglas;
	}
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getHorasLectivas() {
		return this.horasLectivas;
	}
	public void setHorasLectivas(int horasLectivas) {
		this.horasLectivas = horasLectivas;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.sigla+" - "+this.nombre+" - "+this.horasLectivas;
	}
}
