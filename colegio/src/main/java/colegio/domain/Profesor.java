package colegio.domain;

import java.util.ArrayList;
import java.util.List;

public class Profesor extends Persona {
	private List<Asignatura> asignaturas;
	private final static int MAX_ASIGNATURAS = 5;

	public Profesor() {
		super();
		this.asignaturas = new ArrayList<Asignatura>(MAX_ASIGNATURAS);
	}

	public List<Asignatura> getAsignaturas() {
		return this.asignaturas;
	}

	public void setAsignaturas(List<Asignatura> asignaturas) {
		this.asignaturas = asignaturas;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Profesor: "+this.getDni()+" - "+this.getNombre()+" - "+this.getApellidos()+
				" - "+this.getSexo()+" - "+this.getEdad();
	}

}
