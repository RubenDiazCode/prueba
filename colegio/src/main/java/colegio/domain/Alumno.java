package colegio.domain;

import java.util.ArrayList;
import java.util.List;

public class Alumno extends Persona {
	private List<Curso> cursos;
	private final static int MAX_CURSOS = 5;

	public Alumno() {
		super();
		this.cursos = new ArrayList<Curso>(MAX_CURSOS);
	}

	public List<Curso> getCursos() {
		return this.cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Alumno: " + this.getDni() + " - " + this.getNombre() + " - " + this.getApellidos() + " - "
				+ this.getSexo() + " - " + this.getEdad();
	}

}
