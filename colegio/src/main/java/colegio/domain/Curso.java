package colegio.domain;

import java.util.Set;

public class Curso {
	private int codigo;
	private String nombre;
	private Set<Asignatura> asignaturasCurso;

	public Curso() {

	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<Asignatura> getAsignaturasCurso() {
		return this.asignaturasCurso;
	}

	public void setAsignaturasCurso(Set<Asignatura> asignaturasCurso) {
		this.asignaturasCurso = asignaturasCurso;
	}

}
