package talent.campus.ejercicio3;

public class IntegerSorter {
	
	private int[] data;

	public IntegerSorter(int[] data) {
		this.data = data;
	}
	
	public void sort() {
		if(this.isEmpy()) {
			return;
		}
		
		for(int i=0; i<this.data.length -2; i++) {
			this.iteration(i);
		}
	}

	private boolean isEmpy() {
		return this.data ==  null || this.data.length == 0;
	}
	
	protected void iteration(int i) {
		for(int j = i +1; j < this.data.length; j++) {
			this.compareAndSwap(i, j);
		}
	}
	
	protected void compareAndSwap(int i, int j) {
		if(this.data[j] < this.data[i]) {
			this.swap(i, j);
		}
	}
	
	protected void swap(int i, int j) {
		int tmp = this.data[i];
		this.data[i] = this.data[j];
		this.data[j] = tmp;
	}
	
	public void print() {
		if(this.isEmpy()) {
			System.out.println("{}");
			return;
		}
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < this.data.length; i++) {
			sb.append(this.data[i]);
			sb.append(",");
		}
		
		String substring = sb.substring(0, sb.length() -1);
		System.out.println("{"+substring+"}");
	}
}
