package talent.campus.ejercicio3;

public class MainApp {

	public static void main(String[] args) {
		int[] data = {55,54,53,52,12,5,1};
		IntegerSorter sorter = new IntegerSorter(data);
		System.out.println("Datos iniciales");
		sorter.print();
		System.out.println();
		
		System.out.println("Prueba swap(2, 3)");
		System.out.println();
		sorter.swap(2, 3);
		sorter.print();
		
		System.out.println("Prueba compareAndSwap(0, 1)");
		System.out.println();
		sorter.compareAndSwap(0, 1);
		sorter.print();
		
		System.out.println("Prueba iteration(0)");
		System.out.println();
		sorter.iteration(0);
		sorter.print();
		
		System.out.println("Prueba sort()");
		System.out.println();
		sorter.sort();
		sorter.print();

	}

}
