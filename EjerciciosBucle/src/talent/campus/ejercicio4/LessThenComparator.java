package talent.campus.ejercicio4;

public interface LessThenComparator {
	
	boolean lessThen(Object item);

}
