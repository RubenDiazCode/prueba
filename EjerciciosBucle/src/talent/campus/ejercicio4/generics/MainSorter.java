package talent.campus.ejercicio4.generics;

public class MainSorter {

	public static void main(String[] args) {
		Integer[] data = {55,54,53,52,12,5,1};
		Sorter<Integer> sorter = new Sorter<Integer>(data);
		System.out.println("Datos iniciales");
		sorter.print();
		System.out.println();
		
		System.out.println("Prueba sort()");
		System.out.println();
		sorter.sort();
		sorter.print();

	}

}
