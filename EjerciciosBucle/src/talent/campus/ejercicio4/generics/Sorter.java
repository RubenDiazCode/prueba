package talent.campus.ejercicio4.generics;

public class Sorter<T extends Comparable<? super T>> {
	
	private T[] data;

	public Sorter(T[] data) {
		this.data = data;
	}
	
	public void sort() {
		if(this.isEmpy()) {
			return;
		}
		
		for(int i=0; i<this.data.length -2; i++) {
			this.iteration(i);
		}
	}

	private boolean isEmpy() {
		return this.data ==  null || this.data.length == 0;
	}
	
	protected void iteration(int i) {
		for(int j = i +1; j < this.data.length; j++) {
			this.compareAndSwap(i, j);
		}
	}
	
	protected void compareAndSwap(int i, int j) {
		if ( this.lessThen(i, j)) {
			this.swap(i, j);
		}
	}

	private boolean lessThen(int i, int j) {
		return this.data[j].compareTo(this.data[i]) == -1;
	}
	
	protected void swap(int i, int j) {
		T tmp = this.data[i];
		this.data[i] = this.data[j];
		this.data[j] = tmp;
	}
	
	public void print() {
		if(this.isEmpy()) {
			System.out.println("{}");
			return;
		}
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < this.data.length; i++) {
			sb.append(this.data[i]);
			sb.append(",");
		}
		
		String substring = sb.substring(0, sb.length() -1);
		System.out.println("{"+substring+"}");
	}
}
