package talent.campus.ejercicio4;

public class Persona implements LessThenComparator{
	
	private String dni;
	
	private String nombre;

	public Persona(String dni, String nombre) {
		super();
		this.dni = dni;
		this.nombre = nombre;
	}

	public String getDni() {
		return this.dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	@Override
	public String toString() {
		return "Persona [dni=" + this.dni + ", nombre=" + this.nombre + "]";
	}

	@Override
	public boolean lessThen(Object item) {
		if( item instanceof Persona) {
			Persona other = (Persona)item;
			return this.dni.compareTo(other.getDni()) == -1;			
		}
		
		return false;
	}

}
