package talent.campus.ejercicio4;



public class MainSorter {

	public static void main(String[] args) {
		Persona[] data = {
				new Persona("002", "Nombre 1"),
				new Persona("003", "Nombre 2"),
				new Persona("002", "Nombre 3"),
				new Persona("001", "Nombre 4")		
		};
		
		
		Sorter sorter = new Sorter(data);
		System.out.println("Datos iniciales");
		sorter.print();
		System.out.println();
		
		System.out.println("Prueba sort()");
		System.out.println();
		sorter.sort();
		sorter.print();


	}

}
