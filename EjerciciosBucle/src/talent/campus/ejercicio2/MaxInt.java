package talent.campus.ejercicio2;

public class MaxInt {

	public static void main(String[] args) {
		int[] integerList = {1,23,1312,12,12,41,41};
		int max = maxInt(integerList);
		System.out.println(max);
	}

	private static int maxInt(int[] integerList) {
		if(integerList ==  null || integerList.length == 0) {
			throw new RuntimeException("El array no puede ser vacío");
		}
		
		int max = integerList[0];
		for(int i = 1; i < integerList.length; i++) {
			if( integerList[i] > max ) {
				max = integerList[i];
			}
		}
		return max;
	}

}
