package talent.campus.ejercicio1;

import java.util.Scanner;

public class Echo {

	public static void main(String[] args) {
		
		System.out.println("Bienvenido al programa Echo. Introduzca un texto o la paralabra SALIR para terminar.");
		
		Scanner scanner = new Scanner(System.in);
		String line = "";
		
		while( noEsSalir(line) ) {		
			line = scanner.nextLine();
			if( noEsSalir(line) ) {
				System.out.println(">>> "+line);
			}
		}
		scanner.close();
		System.out.println("Programa echo parado.");

	}

	private static boolean noEsSalir(String line) {
		return ! "salir".equalsIgnoreCase(line);
	}

}
