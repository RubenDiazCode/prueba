package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "profesor")
public class Profesor {
	public Profesor() {

	}

	public Profesor(String dni, String nombre, String apellidos, Integer anyoNac) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.anyoNac = anyoNac;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "dni")
	private String dni;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apellidos")
	private String apellidos;

	@Column(name = "anyo_nac")
	private Integer anyoNac;

	// n:1 aqui, hacer el 1:n en el otro lado
	@ManyToOne
	@JoinColumn(name = "asignatura_id")
	private Asignatura asignatura;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getAnyoNac() {
		return this.anyoNac;
	}

	public void setAnyoNac(Integer anyoNac) {
		this.anyoNac = anyoNac;
	}

	public Asignatura getAsignatura() {
		return this.asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "profesor: " + this.id + " - " + this.nombre + " - " + this.anyoNac;
	}

}
