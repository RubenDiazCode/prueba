package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "alumno")
public class Alumno {
	public Alumno() {
	}
	

	public Alumno(String dni, String nombre, String apellidos, Integer anyoNac) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.anyoNac = anyoNac;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "dni")
	private String dni;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apellidos")
	private String apellidos;

	@Column(name = "anyo_nac")
	private Integer anyoNac;

	@ManyToMany // relacion m:n con asignaturas
	@JoinTable(name = "asignaturaalumno",
	joinColumns = { @JoinColumn(name = "Alumno_id")}, 
	inverseJoinColumns = { @JoinColumn(name = "Asignatura_id")})
	private List<Asignatura> asignaturas = new ArrayList<Asignatura>();

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getAnyoNac() {
		return this.anyoNac;
	}

	public void setAnyoNac(Integer anyoNac) {
		this.anyoNac = anyoNac;
	}

	public Collection<Asignatura> getAsignaturas() {
		return this.asignaturas;
	}

	public void setAsignaturas(List<Asignatura> asignaturas) {
		this.asignaturas = asignaturas;
	}
	
	public void addAsignatura(Asignatura asignatura) {
		this.asignaturas.add(asignatura);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "alumno: "+this.id+" - "+this.nombre+" - "+this.anyoNac;
	}

}
