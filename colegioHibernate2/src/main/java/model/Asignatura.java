package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "asignatura")

public class Asignatura {
	public Asignatura() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "horas_semanales")
	private Integer horasSemanales;

	@ManyToMany(mappedBy = "asignaturas") // relacion n:m hecha en alumno, mirar alli
	private List<Alumno> alumnos = new ArrayList<Alumno>();
	
	//1:n aqui, hacer el n:1 en el otro lado
	@OneToMany(mappedBy ="asignatura")
	private List<Profesor> profesores = new ArrayList<Profesor>();

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getHorasSemanales() {
		return this.horasSemanales;
	}

	public void setHorasSemanales(Integer horasSemanales) {
		this.horasSemanales = horasSemanales;
	}

	public Collection<Alumno> getAlumnos() {
		return this.alumnos;
	}

	public void setAlumnos(List<Alumno> alumnos) {
		this.alumnos = alumnos;
	}

	public List<Profesor> getProfesores() {
		return this.profesores;
	}

	public void setProfesores(List<Profesor> profesores) {
		this.profesores = profesores;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "asignatura: "+this.nombre+" - "+this.horasSemanales+" horas";
	}
}
