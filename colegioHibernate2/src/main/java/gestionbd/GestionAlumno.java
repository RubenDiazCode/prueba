package gestionbd;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.Alumno;
import model.Asignatura;

public class GestionAlumno {

	private Session session;

	public GestionAlumno(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public List<Alumno> findAll() {
		Query<Alumno> query = this.session.createQuery("SELECT a FROM Alumno a",Alumno.class);
		return query.list();

	}
	
	public List<Alumno> findbyDNI(String dni) {
		Query<Alumno> query=this.session.createQuery("SELECT a FROM Alumno a WHERE a.dni = :dni",Alumno.class);
		query.setParameter("dni", dni);
		return query.list();
	}
	//insert alumno
	public void insert(Alumno alumno) {
		this.session.save(alumno);
	}
	
	//update de rafa
	public int updateQuery(Alumno alumno) {
		Query<Alumno> query = this.session.createQuery("UPDATE Alumno a SET a.nombre = :nombre WHERE a.id = :id",Alumno.class);
	    query.setParameter("id",alumno.getId());
	    query.setParameter("nombre",alumno.getNombre());
	    return query.executeUpdate();		
	}
	
	//vincular alumnos y asignaturas
	public void updateAlumno(Alumno a, Asignatura asig) {
		a.addAsignatura(asig);
		this.session.save(a);
	}
	
	
}
