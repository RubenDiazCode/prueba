 package gestionbd;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.Asignatura;

public class GestionAsignatura {
	private Session session;

	public GestionAsignatura(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public List<Asignatura> findAll() {
		Query<Asignatura> query = this.session.createQuery("SELECT a FROM Asignatura a", Asignatura.class);
		return query.list();
	}

	public List<Asignatura> findLikeNombre(String nombre) {
		Query<Asignatura> query = this.session.createQuery("select a from Asignatura a where a.nombre like :nombre",
				Asignatura.class);
		query.setParameter("nombre", "%"+nombre+"%");
		return query.list();
	}
	
	public List<Asignatura> findPorAlumnoDNI(String dni){
		Query<Asignatura> query= this.session.createQuery("select a from Asignatura a "+
									"join a.alumnos al where al.dni = :dni",Asignatura.class);
		query.setParameter("dni", dni);
		return query.list();
	}

}
