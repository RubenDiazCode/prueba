package gestionbd;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.Asignatura;
import model.Profesor;

public class GestionProfesor {
	private Session session;

	public GestionProfesor(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
	public List<Profesor>findAll(){
		Query<Profesor> query=this.session.createQuery("select p from Profesor p",Profesor.class);
		return query.list();
	}
	
	public List<Profesor>findMayores(Integer edad){
		Query<Profesor> query=this.session.createQuery("select p from Profesor p where year(SYSDATE()) - p.anyoNac > :edad",Profesor.class);
		query.setParameter("edad", edad);
		return query.list();		
	}
	
	public void insert(Profesor p){
		this.session.save(p);
	}
	
	public void updateProfe(Asignatura asig, Profesor p) {
		p.setAsignatura(asig);
		this.session.save(p);	
		asig.getProfesores().add(p);
		this.session.save(asig);
	}
	

}
