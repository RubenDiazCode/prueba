package main;

import java.io.IOException;
import java.util.List;

import org.hibernate.Session;

import gestionbd.GestionAlumno;
import gestionbd.GestionAsignatura;
import gestionbd.GestionProfesor;
import model.Alumno;
import model.Asignatura;
import model.Profesor;
import utils.HibernateUtils;

public class Main {

	public static void main(String[] args) throws IOException {
		Session session = HibernateUtils.getSessionFactory().openSession();
		try {
			session.beginTransaction();
//asignaturas
			GestionAsignatura gas = new GestionAsignatura(session);
			List<Asignatura> listAsig = gas.findPorAlumnoDNI("666");
			Asignatura asig = listAsig.get(0);
			System.out.println(asig.toString());

			GestionAlumno ga = new GestionAlumno(session);
			List<Alumno> al = ga.findbyDNI("666");
			System.out.println(al.toString());
			Alumno alUpdate = al.get(0);
			
//			ga.updateAlumno(alUpdate, asig);
//			Alumno nuevo=new Alumno("4654654f","galans","kavron",1234);		
//			ga.insert(nuevo);
			// List<Alumno> lista=ga.findAll();
			for (Alumno a : al) {
				System.out.println(a.toString());
			}
			GestionProfesor gp = new GestionProfesor(session);
//			Profesor p=new Profesor("464564s","profe","chulo",1994);
//			gp.insert(p);
//			gp.findMayores(18).forEach(pro -> System.out.println(pro.toString()));
//			Profesor p = gp.findMayores(18).get(0);
//			System.out.println(p.toString());
//
//			gp.updateProfe(asig, p);
			session.getTransaction().commit();
//			System.out.println(p.toString());

			// session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
			HibernateUtils.shutdown();
		}
	}

}
